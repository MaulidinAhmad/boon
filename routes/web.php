<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/book', 'HomeController@book');
Route::get('item/detail/{id}', 'HomeController@itemDetail')->name('item.detail');
Route::get('item/categories/{id}/{name}', 'HomeController@categories')->name('item.categories');


Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function (){

    Route::group(['prefix' => 'auth', 'as' => 'admin.'], function(){
       Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
       Route::post('login', 'Auth\LoginController@login');
       Route::post('logout', 'Auth\LoginController@logout')->name('logout');

       Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('forgot');
       Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
       Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('reset');
       Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.request');
    });

    # Kalau sudah login
    Route::group(['middleware' => 'admin'], function (){
        Route::get('home', 'MainController@index')->name('home');
        Route::resource('user', 'UserController', ['except' => ['show']]);
        Route::get('profile', ['as' => 'admin.profile.edit', 'uses' => 'ProfileController@edit']);
        Route::put('profile', ['as' => 'admin.profile.update', 'uses' => 'ProfileController@update']);
        Route::put('profile/password', ['as' => 'admin.profile.password', 'uses' => 'ProfileController@password']);

        Route::group(['namespace' => 'Item'], function (){
            Route::group(['prefix' => 'listing'], function(){
                Route::get('form', 'ItemController@showAddForm')->name('admin.listing.form');
                Route::post('form', 'ItemController@addForm');
                Route::get('edit/{id}', 'ItemController@editItem')->name('admin.listing.edit');
                Route::post('update', 'ItemController@updateItem')->name('admin.listing.update');
                Route::get('delete/{id}', 'ItemController@deleteItem')->name('admin.listing.delete');
                Route::post('photos/{id?}/delete', 'ItemController@stockPhotoDelete')->name('admin.listing.photo.delete');
                Route::post('get_city}', 'ItemController@getCity')->name('admin.listing.getCity');
            });
            Route::group(['prefix' => 'item'], function(){
                Route::get('list',  'ItemController@index')->name('admin.item');
                Route::get('activate/{id}',  'ItemController@activateItem')->name('admin.item.activate');
                Route::get('deactivate/{id}',  'ItemController@deactivateItem')->name('admin.item.deactivate');
            });
        });
        Route::group(['prefix' => 'transaction'], function(){
            Route::get('list',  'TransactionController@index')->name('admin.transaction');            
            Route::get('detail/{id}',  'TransactionController@detail')->name('admin.transaction.detail');            
            Route::get('set_payed/{id}',  'TransactionController@setPayed')->name('admin.transaction.setPayed');
            Route::get('set_notpayed/{id}',  'TransactionController@setNotPayed')->name('admin.transaction.setNotPayed');
            Route::get('set_canceled/{id}',  'TransactionController@setCanceled')->name('admin.transaction.setCanceled');            
        });

    });

});


// User Registered
Route::group(['prefix' => 'user', 'namespace' => 'User'], function (){
    Route::group(['prefix' => 'auth', 'as' => 'user.'], function(){
      Auth::routes();
    });

    // Route::get('password/forgot', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
    // Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('send.password.email');
    // Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    // Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('user.password.request');
    // Route::get('activate/{code?}', 'MainController@activateAccount')->name('user.account.activate');
    // Route::post('activate/{code?}', 'MainController@activateAccountPost')->name('user.account.activate.update');
    // Route::get('message/list', 'MainController@messageList')->name('user.message.list');

    // Loged In
    Route::group(['middleware' => 'user'], function (){
        // Route::resource('user', 'UserController', ['except' => ['show']]);
        Route::get('profile', ['as' => 'user.profile.edit', 'uses' => 'ProfileController@edit']);
        Route::put('profile', ['as' => 'user.profile.update', 'uses' => 'ProfileController@update']);
        Route::put('profile/password', ['as' => 'user.profile.password', 'uses' => 'ProfileController@password']);
        Route::get('home', 'MainController@index')->name('user.home');
        Route::get('home/{id?}', 'MainController@index')->name('user.home.read');
        Route::post('home', 'MainController@updateProfile')->name('user.updateprofile');

        // Items Route
            Route::group(['namespace' => 'Item'], function (){
                Route::group(['prefix' => 'listing'], function(){
                    Route::get('form', 'ItemController@showAddForm')->name('user.listing.form');
                    Route::post('form', 'ItemController@addForm');
                    Route::get('edit/{id}', 'ItemController@editItem')->name('user.listing.edit');
                    Route::post('update', 'ItemController@updateItem')->name('user.listing.update');
                    Route::get('delete/{id}', 'ItemController@deleteItem')->name('user.listing.delete');
                    Route::post('photos/{id?}/delete', 'ItemController@stockPhotoDelete')->name('user.listing.photo.delete');
                });
                Route::group(['prefix' => 'item'], function(){
                    Route::get('list',  'ItemController@index')->name('user.item');
                });
            });
        
        // Sales Route
        Route::group(['prefix' => 'sales'], function(){
                Route::get('list', 'SalesController@index')->name('user.transaction.sales');                
                Route::get('detail{id}', 'SalesController@detail')->name('user.transaction.sales.detail');                
            });            
        // Order Route
        Route::group(['prefix' => 'order'], function(){
                Route::get('list',  'OrderController@index')->name('user.transaction.order');    
                Route::get('detail{id}',  'OrderController@detail')->name('user.transaction.order.detail');    
            });                                 

    });

    // Cart
    Route::group(['prefix' => 'cart'], function () {
        Route::get('list','CartController@index')->name('user.cart.list');
        Route::post('add','CartController@add')->name('user.cart.add');
        Route::get('delete/{id}','CartController@delete')->name('user.cart.delete');
        Route::get('inc_quantity/{id}','CartController@incQuantity')->name('user.cart.inc');
        Route::get('dec_quantity/{id}','CartController@decQuantity')->name('user.cart.dec');
    });

    // Checkout
    Route::group(['prefix' => 'checkout'], function () {
        // Route::get('/',  'CheckoutController@index')->name('user.checkout.list');
        Route::get('add/{id}',  'CheckoutController@index')->name('user.checkout.add');
        Route::post('get_city',  'CheckoutController@getCity')->name('user.checkout.add.getCity');
        Route::post('get_ongkir',  'CheckoutController@getOngkir')->name('user.checkout.add.getOngkir');
        Route::post('checkouted',  'CheckoutController@checkouted')->name('user.checkout.checkouted');
        Route::post('checkout/payment',  'CheckoutController@payment')->name('user.checkout.payment');
        Route::get('checkout/print_invoice/{id}',  'CheckoutController@invoice')->name('user.checkout.invoice');
    });   


});

