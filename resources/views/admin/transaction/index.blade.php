@extends('admin.layouts.app', ['activePage' => 'transaction', 'titlePage' => __('Transaction')])
@section('content')
{{-- <div class="row">
    <div class="col-md-12"> --}}
        @foreach ($data as $data)                    
        <div class="row justify-content-center" style="margin-bottom:-50px;">
            <div class="col-md-9">
                <div class="card card-nav-tabs">                    
                    <div class="card-body">
                    <h4 class="card-title mb-3">{{$data->code}}
                        @if ($data->transactionDetail->status == "0")
                        <span class="">                            
                        <button class="btn float-right btn-sm btn-warning">Not Payed</button></span>
                        @elseif($data->transactionDetail->status == "1")
                        <span class="ml-4"><button class="btn float-right btn-sm btn-success">Payed</button></span>
                        @else
                        <span class="ml-4"><button class="btn float-right btn-sm btn-danger">Canceled</button></span>
                        @endif
                        </h4>                      
                        
                          <a href="{{$data->detail_link}}" class="btn btn-sm btn-info mt-3">Detail</a>                      
                          <i class="material-icons float-right" style="margin-top:20px;cursor:pointer;" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">settings</i>                                                    
                          <div class="dropdown" style="margin-top:-100px;">
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{$data->payed_link}}">Set Payed</a>
                                    <a class="dropdown-item" href="{{$data->notpayed_link}}">Set Not Payed</a>
                                    <a class="dropdown-item" href="{{$data->canceled_link}}">Set Canceled</a>                                    
                                </div>
                            </div>                                            
                        
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    {{-- </div>
  </div> --}} 
@endsection
@push('js')
    <script>
        $.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
    });        
    </script>   
@endpush
