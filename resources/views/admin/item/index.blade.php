@extends('admin.layouts.app', ['activePage' => 'item', 'titlePage' => __('Items')])
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-warning">
            <h4 class="card-title ">{{ __('Items') }}</h4>
            <p class="card-category"> {{ __('Manage Your Items') }}</p>
          </div>
          <div class="card-body">
            @if (session('status'))
              <div class="row">
                <div class="col-sm-12">
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span>{{ session('status') }}</span>
                  </div>
                </div>
              </div>
            @endif
            <div class="row">
              <div class="col-12 text-right">
                <a href="{{route('admin.listing.form')}}" class="btn btn-sm btn-info">{{ __('Add items') }}</a>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table">
                <thead class=" text">
                  <th>
                      {{ __('Name') }}
                  </th>                  
                  <th>
                    {{ __('Quantity') }}
                  </th>
                  <th>
                    {{ __('Price') }}
                  </th>                  
                  <th>
                    {{ __('Status') }}
                  </th>                  
                  <th>
                    {{ __('Creation date') }}
                  </th>
                  <th class="text-right">
                    {{ __('Actions') }}
                  </th>
                </thead>
                <tbody>
                  
                  @foreach($items as $item)
                    <tr>
                      <td>
                        {{ $item->name }}
                      </td>
                      <td>
                        {{ $item->quantity }}
                      </td>
                      <td>
                        {{ $item->price }}
                      </td>
                      <td>
                          @if ($item->is_published == 0)
                          <button type="button" rel="tooltip" class="btn btn-sm btn-danger">
                              Not Active
                            </button>  
                          @elseif($item->is_published == 1)
                          <button type="button" rel="tooltip" class="btn btn-sm btn-success">
                              Active
                            </button>  
                          @endif
                      </td>
                      <td>
                          {{ $item->created_at}}
                        </td>
                      <td class="td-actions text-right">
                      <a href="{{$item->edit_link_admin}}">
                          <button type="button" rel="tooltip" class="btn btn-info btn-round">
                            <i class="material-icons">edit</i>
                          </button>
                        </a>
                        <a href="{{route('admin.listing.delete',['id'=>$item->id])}}">
                          <button type="button" rel="tooltip" class="btn btn-danger btn-round">
                            <i class="material-icons">close</i>
                          </button>
                        </a>
                        @if ($item->is_published == 0)
                        <a href="{{route('admin.item.activate',['id'=>$item->id])}}">
                            <button type="button" rel="tooltip" class="btn btn-success btn-sm">
                              <i class="material-icons">check</i>Activate
                            </button>
                        </a>
                        @elseif($item->is_published == 1)
                        <a href="{{route('admin.item.deactivate',['id'=>$item->id])}}">
                            <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                              <i class="material-icons">close</i>Deactivate
                            </button>
                        </a>
                        @endif           
                        </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="row">
                <div class="col-md-7 center">
                  {{$items->links()}}
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
@endsection
