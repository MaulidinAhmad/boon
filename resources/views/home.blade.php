@extends('user.layouts.app')
@section('content')

@include('user.layouts.carousel')

<section>
    <div class="container">
        <div class="row">
        @include('user.layouts.sidebar')
            
            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Features Items</h2>                                                                
                    {{-- {{dd($items)}} --}}
                    @if (!$items->isEmpty())
                        
                    
                    @foreach ($items as $data)                        
                    <div class="col-sm-4">                        
                        <div class="product-image-wrapper">
                            <form style="display:none" id="form-{{$data->id}}" action="{{route('user.cart.add', ['id' => $data->id])}}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="text" name="id" value="{{$data->id}}">                                        
                            </form>
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="{{$data->pict}}" alt="" />
                                            <h2>Rp. {{number_format($data->price,2,",",",")}}</h2>
                                        <a href="{{route('item.detail',['id'=>$data->id])}}"><p>{{$data->name}}</p></a>
                                        <a href="" onclick="addToCart('{{$data->id}}')" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </div>
                                    <div class="product-overlay">
                                        <div class="overlay-content">
                                            <h2>{{number_format($data->price,2,",",",")}}</h2>
                                            <a href="{{route('item.detail',['id'=>$data->id])}}"><p>{{$data->name}}</p></a>
                                            <a  onclick="addToCart('{{$data->id}}')" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            <div class="choose">
                                <ul class="nav nav-pills nav-justified">
                                <li><a href="{{route('user.checkout.add',['id'=>http_build_query(array("id"=>$data->id))])}}"><i class="fa fa-plus-square"></i>Buy Now</a></li>
                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>                    
                    @endforeach   
                    @endif                 
                    
                    
                </div><!--features_items-->
                                               
                
            </div>
        </div>
    </div>
</section>
@endsection
@push('js')
<script>
function addToCart(id){
        // var user_is_logged = "{{ (Auth::check()) ? 1 : 0 }}";
        // if(user_is_logged == "0"){
        //     swal({ type: 'error', title: "Failed add to cart!", text: "You must login first!" });
        // } else {
                // function(inputValue){

                //     if (inputValue === false) return false;

                //     if(inputValue !== false){                        
                        $('#form-'+id).closest('form').submit();
                //     }
                // });
        // }
    }
</script>    
@endpush