@extends('user.layouts.app')
@section('content')
<section>
    <div class="container">
        <div class="row">
        @include('user.layouts.sidebar')            
            
            <div class="col-sm-9 padding-right">
                <div class="product-details"><!--product-details-->
                    <div class="col-sm-5">
                        <div class="view-product">
                        <img style="max-width:33S0px;max-height:310px" src="{{$item->pict}}" alt="" />
                            {{-- <h3>ZOOM</h3> --}}
                        </div>                        

                    </div>
                    <div class="col-sm-7">
                        <div class="product-information" style="padding:20px 0px 20px 20px"><!--/product-information-->
                            {{-- <img src="{{$item->pict}}" class="newarrival" alt="" /> --}}
                            <form style="display:none" id="form-{{$item->id}}" action="{{route('user.cart.add', ['id' => $item->id])}}" method="POST">
                                {{ csrf_field() }}
                                <input type="text" name="id" value="{{$item->id}}">                                        
                        </form>
                            <h2>{{$item->name}}</h2>
                            <p></p>
                            <img src="images/product-details/rating.png" alt="" />
                            <span>
                                <span>{{number_format($item->price,2,",",".")}}</span>
                                <label>Quantity:</label>
                                
                            <input type="text"  value="{{$item->quantity}}" />
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{route('user.checkout.add',['id'=>$item->id])}}"><button style="" type="button"  class="btn btn-fefault cart">
                                                <i class="fa fa-shopping-cart"></i>
                                                Buy
                                    </button>
                                </a>
                                    <button style="" type="button" onclick="addToCart('{{$item->id}}')" class="btn btn-fefault cart">
                                        <i class="fa fa-shopping-cart"></i>
                                        Add to cart
                                    </button>
                                </div>
                                
                            </div>
                                
                            </span>
                            <p><b>Availability:</b> In Stock</p>
                            <p><b>Condition:</b> New</p>
                            <p><b>Location:</b> {{$item->location->country->name}}, {{$item->location->province->province}}</p>
                            <p><b>Brand:</b> E-SHOPPER</p>                            
                        </div><!--/product-information-->
                    </div>                    
                            <div class="col-sm-9" style="padding-top:10px;">
                                <p><b>Description : </b></p>                                
                                <p>{{$item->special_condition}}</p>
                            </div>
                        
                </div><!--/product-details-->                
                
                <div class="category-tab shop-details-tab"><!--category-tab-->
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            <li><a href="#details" data-toggle="tab">Details</a></li>
                            <li><a href="#companyprofile" data-toggle="tab">Company Profile</a></li>
                            <li><a href="#tag" data-toggle="tab">Tag</a></li>
                            <li class="active"><a href="#reviews" data-toggle="tab">Reviews (5)</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade" id="details" >
                            
                        </div>
                        
                        <div class="tab-pane fade" id="companyprofile" >
                            
                        </div>
                        
                        <div class="tab-pane fade" id="tag" >
                            
                        </div>
                        
                        <div class="tab-pane fade active in" id="reviews" >
                            <div class="col-sm-12">
                                <ul>
                                <li><a href=""><i class="fa fa-user"></i>HMM</a></li>
                                    <li><a href=""><i class="fa fa-clock-o"></i>12:41 PM</a></li>
                                    <li><a href=""><i class="fa fa-calendar-o"></i>31 DEC 2014</a></li>
                                </ul>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                <p><b>Write Your Review</b></p>
                                
                                <form action="#">
                                    <span>
                                        <input type="text" placeholder="Your Name"/>
                                        <input type="email" placeholder="Email Address"/>
                                    </span>
                                    <textarea name="" ></textarea>
                                    <b>Rating: </b> <img src="images/product-details/rating.png" alt="" />
                                    <button type="button" class="btn btn-default pull-right">
                                        Submit
                                    </button>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </div><!--/category-tab-->                                
                
            </div>
        </div>
    </div>
</section>

@endsection
@push('js')
<script>
function addToCart(id){
        // var user_is_logged = "{{ (Auth::check()) ? 1 : 0 }}";
        // if(user_is_logged == "0"){
        //     swal({ type: 'error', title: "Failed add to cart!", text: "You must login first!" });
        // } else {
                // function(inputValue){

                //     if (inputValue === false) return false;

                //     if(inputValue !== false){                        
                        $('#form-'+id).closest('form').submit();
                //     }
                // });
        // }
    }
</script>    
@endpush