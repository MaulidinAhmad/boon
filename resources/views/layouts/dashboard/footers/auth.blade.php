<footer class="footer">
  <div class="container-fluid">
    <nav class="float-left">
    </nav>
    <div class="copyright float-right">
      &copy;
      <script>
        document.write(new Date().getFullYear())
      </script>, BON Inc.
    </div>
  </div>
</footer>