@extends('user.layouts.app')
@section('content')
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				<li><a href="{{url('/')}}">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>			
			@if(\Cart::getContent()->count() > 0)
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						@php							
							$id_items=array();
						@endphp
						@foreach (Cart::getContent() as $item)													
						@php
							$photo=App\Models\Item::where('id',$item->id)->first();
						@endphp
						<tr>														
							<td class="cart_product">
									<a href=""><img style="max-width:251px;" src="{{$photo->pict}}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$item->name}}</a></h4>
							<p>ID: {{$item->id}}</p>
							</td>
							<td class="cart_price">
							<p>{{$item->price}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href="{{route('user.cart.inc',["id"=>$item->id])}}"> + </a>
								<input class="cart_quantity_input" type="text" name="quantity" value="{{$item->quantity}}" autocomplete="off" size="2">
									<a class="cart_quantity_down" href="{{route('user.cart.dec',["id"=>$item->id])}}"> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">Rp. {{number_format(Cart::get($item->id)->getPriceSum(),2,',','.')}}</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="{{route('user.cart.delete',["id"=>$item->id])}}"><i class="fa fa-times"></i></a>
							</td>
						</tr>
						@php
							$id_items[]=[$item->id];							
						@endphp
						@endforeach
						@php
							$id_items=http_build_query(array("id"=>$id_items))
						@endphp
					</tbody>
				</table>				
			</div>
			<div class="row">
				<div class="col-md-12">
					{{-- {{dd($id_items)}} --}}
					<a href="{{route('user.checkout.add',["id"=>$id_items])}}" >
						<button style="" type="" onclick="" class="btn btn-fefault cart">
								<i class="fa fa-crosshairs"></i>
								Checkout
							</button>	
						</a>
				</div>
			</div>
			@endif
		</div>
	</section> <!--/#cart_items-->

	
@endsection