<div class="col-sm-3">
        <div class="left-sidebar">
            <h2>Category</h2>
            <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                {{-- {{App\Models\Item::where('id_category',$category->id)->count()}} --}}
                @foreach (App\Models\ItemCategory::groupBy('id_category')->get() as $category)
                    
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                        <a  data-parent="#accordian" href="{{route('item.categories',["id"=>$category->id_category,"name"=>$category->category[0]['category_name']])}}">
                                <span class="badge pull-right">({{App\Models\ItemCategory::where('id_category',$category->id_category)->with('items')->count()}})</span>
                                {{$category->category[0]['category_name']}}
                            </a>
                        </h4>
                    </div>                    
                </div>
                @endforeach
                
            </div><!--/category-products-->
        
            
            {{-- <div class="price-range"><!--price-range-->
                <h2>Price Range</h2>
                <div class="well text-center">
                     <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
                     <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
                </div>
            </div><!--/price-range--> --}}
                        
        
        </div>
    </div>