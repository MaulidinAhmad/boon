@extends('user.layouts.app')
@section('content')
@php
	$total_item=count($items);
	$totals=0;
@endphp		
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Checkout</li>
				</ol>
			</div><!--/breadcrums-->			
			

			<div class="shopper-informations">
				<div class="row">					
					<div class="col-sm-5 clearfix">
						<div class="bill-to">
							<p>Bill To</p>							
							<div class="form-two">
							<form method="post" action="{{route('user.checkout.checkouted')}}">	
									@csrf			
									@if ($total_item > 1)
									<input type="hidden" name="id_item[]" value="{{$id}}">		
									@else
										
									<input type="hidden" name="id_item" value="{{$id}}">		
									@endif			

									<select name="country_location">
										<option>-- Country --</option>
										@foreach ($country as $country)
											<option value="{{$country->name}}">{{$country->name}}</option>
										@endforeach
									</select>
									<select name="province_location" id="province-select">
										<option>-- Province --</option>										
										@foreach ($province as $key=>$provinces)
										<option value="{{$provinces['province_id']}}">{{$provinces['province']}}</option>
										@endforeach
									</select>	
									<select name="city_location" id="city-select">
										<option>-- City --</option>																				
									</select>		
									<input type="text" name="postal_code" id="postal-code" placeholder="Zip / Postal Code *">									
									<input type="text" name="number_phone" placeholder="Phone *">									
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="order-message">
							<p>Other Description</p>
							<textarea name="description"  placeholder="Notes about your order, Special Notes for Delivery" rows="16"></textarea>
							{{-- <label><input type="checkbox"> Shipping to bill address</label> --}}
						</div>	
					</div>					
				</div>
			</div>
			<div class="review-payment">
				<h2>Review & Payment</h2>
			</div>
			
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							{{-- <td></td> --}}
						</tr>
					</thead>
					<tbody>										
						@foreach ($items as $key => $items)	
						@php
							$key++;							
						@endphp												
						<tr>
							<td class="cart_product">
								<a href=""><img style="max-width:251px;" src="{{$items->pict}}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$items->name}}</a></a></h4>
								<p></p>
							</td>
							<td class="cart_price">
								<p>Rp. {{number_format($items->price,2,",",".")}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" id="incquantity" onclick="incQuantity({{$key}})" href="javascript: void(0)"> + </a>
									@if ($total_item > 1)																		
									<input class="cart_quantity_input" id="quantity{{$key}}" type="text" name="quantity" value="{{Cart::get($items->id)->quantity}}" autocomplete="off" size="2">	
									@else										
									<input class="cart_quantity_input" id="quantity" type="text" name="quantity" value="1" autocomplete="off" size="2">
									@endif
									<a class="cart_quantity_down" id="decquantity" href="javascript: void(0)"onclick="decQuantity({{$key}})"> - </a>
								</div>
							</td>
							<td class="cart_total">	
							@if ($total_item > 1)						
							<p id="total_item{{$key}}" hargatotal{{$key}}="{{$items->price * Cart::get($items->id)->quantity}}" harga{{$key}}="{{$items->price}}" class="cart_total_price">Rp.{{number_format($items->price*Cart::get($items->id)->quantity,2,',','.')}}</p>
							@else
							<p id="total_item" hargatotal="{{$items->price}}" harga="{{$items->price}}" class="cart_total_price">Rp.{{number_format($items->price,2,',','.')}}</p>
							@endif
							</td>							
						</tr>						
					</tbody>
					@php					
						$totals+=(int)Cart::get($items->id)->quantity * (int)$items->price;						
					@endphp
					@endforeach
				</table>
			</div>
			<h2>Courier</h2>
			<div class="table-responsive cart_info" id="table_courier">				
				{{-- <table class="table table-condensed" style="width:100%;">
					<thead>
						<tr class="cart_menu" id="">
							<td></td>
							<td id="curier-name"></td>
							<td></td>
							<td></td>
						</tr>
					</thead>
					<tbody id="curier-cost">							
					</tbody>
				</table>								 --}}
			</div>
			<div class="row">
				<div class="col-md-8">
					<h2>Total</h2>
				</div>
				<div class="col-md-4">
				<h2 id="totalhargaall">Rp. {{number_format($totals,2,',','.')}}</h2>
					<input type="hidden" value="{{$totals}}" id="input_total_checkout" name="total_checkout">
				</div>
			</div>
			<div class="row">	
				<div class="col-md-12">
					<span>
						{{-- <label><input type="checkbox"> Direct Bank Transfer</label> --}}
					</span>
				</div>
			</div>
			<div class="row" style="margin-top:8px;">
					<div class="col-md-10">
						<button style="" type="submit" onclick="" class="btn btn-fefault cart">
								<i class="fa fa-crosshairs"></i>
								Checkout
							</button>	
						</div>	
					</div>				
				</div>					
			</div>
		</form>
	</section> <!--/#cart_items-->
	@endsection
	@push('js')
	<script>
$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});


$("select#province-select").change(function () { 	 
	var id = $(this).children("option:selected").val();	
	$.ajax({
	type: "post",
	url: "{{route('user.checkout.add.getCity')}}",
	data: {
		id:id,
	},
	dataType: "json",
	success: function (data) {		
		var provinces="<option>--City--</option>";
		for (let i = 0; i < data.length; i++) {
			provinces += '<option postal="'+data[i].postal_code+'" value="'+data[i].city_id+'">'+data[i].city_name+'</option>' ;			
		}
		$("select#city-select").html(provinces);		
	}
});	
});

$("select#city-select").change(function () { 	 
	var postal = $(this).children("option:selected").attr('postal');	
	var city_id = $(this).children("option:selected").val();	
	$('#postal-code').val(postal);	
	var id_item="{{$items->id}}";
	$.ajax({
		type: "post",
		url: "{{route('user.checkout.add.getOngkir')}}",
		data: {
			city_id:city_id,
			id_item:id_item,
		},
		dataType: "json",
		success: function (data) {				
			var curierSub="";
			var table="";			
						
			for (let c = 0; c < data.length; c++) {	
				for (let d = 0; d < data[c].cost.costs.length; d++) {										
					table += '<table class="table table-condensed" style="width:100%;">'+
						'<thead>'+
						'<tr class="cart_menu" id="">'+	
						'<td>'+'</td>'+
						'<td id="curier-name">'+data[c].cost.name+'</td>'+
						'<td>'+'</td>'+
						'<td>'+'</td>'+
						'</tr>'+
						'</thead>'+												
						'<tbody id="curier-cost">'+
							'<tr>'+'<td><input type="checkbox" id="selected-ongkir"></td>'+'<td class="cart_product" style="width:60%;"><h4>'+data[c].cost.costs[d].service+' ('+data[c].cost.costs[d].description+')'+'</h4></td>'+
							'<td class="cart_description" style="width:20%;"><h4>'+data[c].cost.costs[d].cost[0].etd+' hari'+'</h4></td>'+
							'<td class="cart_price" style="width:20%;" ongkir="'+data[c].cost.costs[d].cost[0].value+'">Rp. '+data[c].cost.costs[d].cost[0].value+'</td>'+
					'</tr>'+
						'</tbody>'
					+'</table>';					
				}
			}				
				$("#table_courier").html(table);
			
		}
	});
});

function currencyFormat(num) {
  return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
}

var quantity=0;
var harga_total_all=0;

if ('{{$total_item}}' > 1) {
	var afterTambah=0;	
function incQuantity(elem){	
	quantity=$("#quantity"+elem).val();
	var hargaAsli=parseInt($("#total_item"+elem).attr('harga'+elem));		
	quantity++	
	$("#quantity"+elem).val(quantity);
	var totalHarga=parseInt(this.quantity) * hargaAsli;
	$("#total_item"+elem).html("Rp. "+currencyFormat(totalHarga))
	$("#total_item"+elem).attr('hargatotal'+elem,totalHarga)
	// console.log(totalHarga)
	$("#input_total_checkout").val(afterTambah	+=totalHarga);
	var akhirHarga=	$("#input_total_checkout").val();
	$("#totalhargaall").html("Rp. "+currencyFormat(parseInt(akhirHarga)))
}

function decQuantity(elem){
	quantity=$("#quantity"+elem).val();	
	if (quantity == 0) {
		return 0;
	}	
	var hargaAsli=parseInt($("#total_item"+elem).attr('harga'+elem));
	quantity--
	console.log(hargaAsli);
	console.log(quantity);
	$("#quantity"+elem).val(quantity);
	var totalHarga=parseInt(this.quantity) * hargaAsli;
	$("#total_item"+elem).html("Rp. "+currencyFormat(totalHarga));
	$("#total_item"+elem).attr('hargaTotal',totalHarga)
	$("#input_total_checkout").val(afterTambah	+=totalHarga);
	var akhirHarga=	$("#input_total_checkout").val();
	$("#totalhargaall").html("Rp. "+currencyFormat(parseInt(akhirHarga)))
}

} else {
	function incQuantity(){	
	quantity=$("#quantity").val();
	var hargaAsli=parseInt($("#total_item").attr('harga'));		
	quantity++	
	$("#quantity").val(quantity);
	var totalHarga=parseInt(quantity) * hargaAsli;
	$("#total_item").html("Rp. "+currencyFormat(totalHarga))
	$("#total_item").attr('hargatotal',totalHarga)
	$("#totalhargaall").html("Rp. "+currencyFormat(totalHarga))
}

function decQuantity(){
	quantity=$("#quantity").val();	
	if (quantity == 0) {
		return 0;
	}	
	var hargaAsli=parseInt($("#total_item").attr('harga'));
	quantity--
	$("#quantity").val(quantity);
	var totalHarga=parseInt(quantity) * hargaAsli;
	$("#total_item").html("Rp. "+currencyFormat(totalHarga));
	$("#total_item").attr('hargaTotal',totalHarga)
	$("#totalhargaall").html("Rp. "+currencyFormat(totalHarga))
}
}


$(document).on('change', '#selected-ongkir', function() {
		var totHarga=parseInt($('#total_item').attr('hargaTotal'));
		var ongkir=parseInt($(this).parent('td').siblings('td.cart_price').attr('ongkir'));		
		var totAll=totHarga+ongkir;				
	if (this.checked) {		
		$('#selected-ongkir').not(this).prop('checked', false);
		$("#totalhargaall").html("Rp. "+currencyFormat(totAll));
		$("#input_total_checkout").val(totAll);		
	}else{		
		$('#selected-ongkir').not(this).prop('checked', false);
		totAll=totAll-ongkir;		
		$("#totalhargaall").html("Rp. "+currencyFormat(totAll))
		$("#input_total_checkout").val(totAll);
	}
});

</script>	
@endpush
