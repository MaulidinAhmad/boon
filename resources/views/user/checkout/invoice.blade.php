<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Invoice</title>
	<link href="{{asset('user')}}/css/bootstrap.min.css" rel="stylesheet">
	<script src="{{asset('user')}}/js/bootstrap.min.js"></script>
	<script src="{{asset('user')}}/js/jquery.js"></script>
<!------ Include the above in your HEAD tag ---------->
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body p-0">
						<div class="row p-5">
							<div class="col-md-6">
								<img style="max-width:220px;" src="{{asset('icon/new/icon.png')}}">
							</div>
	
							<div class="col-md-6 text-right">
								<p class="font-weight-bold mb-1">Invoice</p>
								<p class="text-muted">{{date('Y-m-d')}}</p>
							</div>
						</div>
	
						<hr class="my-5">
	
						<div class="row pb-5 p-5">
							<div class="col-md-6">
								<p class="font-weight-bold mb-4">Client Information</p>
							<p class="mb-1">{{$data->transaction->buyer->name}}</p>
								{{-- <p>Acme Inc</p> --}}
							<p class="mb-1">{{$data->address_province}}, {{$data->address_country}}</p>
							<p class="mb-1">{{$data->address_postal}}</p>
							</div>
	
							{{-- <div class="col-md-6 text-right">
								<p class="font-weight-bold mb-4">Payment Details</p>
								<p class="mb-1"><span class="text-muted">VAT: </span> 1425782</p>
								<p class="mb-1"><span class="text-muted">VAT ID: </span> 10253642</p>
								<p class="mb-1"><span class="text-muted">Payment Type: </span> Root</p>
								<p class="mb-1"><span class="text-muted">Name: </span> John Doe</p>
							</div> --}}
						</div>
	
						<div class="row p-5">
							<div class="col-md-12">
								<table class="table">
									<thead>
										<tr>
											<th class="border-0 text-uppercase small font-weight-bold">ID</th>
											<th class="border-0 text-uppercase small font-weight-bold">Item</th>
											<th class="border-0 text-uppercase small font-weight-bold">Description</th>
											<th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
											<th class="border-0 text-uppercase small font-weight-bold">Unit Cost</th>
											<th class="border-0 text-uppercase small font-weight-bold">Total</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>{{$data->item->id}}</td>
											<td>{{$data->item->name}}</td>
										<td>{{$data->address_detail}}</td>
										<td>{{$data->quantity}}</td>
											<td>Rp. {{number_format($data->price,2,".",",")}}</td>
											<td>Rp. {{$data->transaction->total}}</td>
										</tr>										
									</tbody>
								</table>
							</div>
						</div>
	
						<div class="d-flex flex-row-reverse bg-dark text-white p-4">
							<div class="py-3 px-5 text-right">
								<div class="mb-2">Grand Total</div>
							<div class="h2 font-weight-light">Rp. {{number_format($data->transaction->total)}}</div>
							</div>
	
							<div class="py-3 px-5 text-right">
								{{-- <div class="mb-2">Discount</div>
								<div class="h2 font-weight-light">10%</div> --}}
							</div>
	
							<div class="py-3 px-5 text-right">
								{{-- <div class="mb-2">Sub - Total amount</div>
								<div class="h2 font-weight-light">$32,432</div> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
	
	</div>
		
</body>
<script>
$(document).ready(function () {
	window.print();
});
</script>
</html>