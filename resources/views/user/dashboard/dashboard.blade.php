@extends('user.dashboard.layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])
@section('content')
<div class="row">    
    <div class="col-lg-4 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-header card-header-success card-header-icon">
          <div class="card-icon">
            <i class="material-icons">done_all</i>
          </div>
          <p class="card-category">Active Items</p>
          <h3 class="card-title">{{\App\Models\Item::where('id_user',Auth::user()->id)->where('is_published','1')->count()}}</h3>
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons">info</i> Total Of Your Active Items
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-header card-header-info card-header-icon">
          <div class="card-icon">
            <i class="material-icons">info</i>
          </div>
          <p class="card-category">Pending Items</p>
          <h3 class="card-title">{{\App\Models\Item::where('id_user',Auth::user()->id)->where('is_published','0')->count()}}</h3>
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons">info</i> Your Pending Items
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-header card-header-warning card-header-icon">
          <div class="card-icon">
            <i class="material-icons">
              warning
            </i>
          </div>
          <p class="card-category">Blocked Items</p>
          <h3 class="card-title">{{\App\Models\Item::where('id_user',Auth::user()->id)->where('is_published','2')->count()}}</h3>
        </div>
        <div class="card-footer">
          <div class="stats">
            <i class="material-icons">info</i> Your Blocked Items
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();
    });
  </script>
@endpush