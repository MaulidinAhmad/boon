@extends('user.dashboard.layouts.app', ['activePage' => 'transaction', 'titlePage' => __('Detail')])
@section('content')
<div class="row justify-content-center" style="margin-bottom:-50px;">
    <div class="col-md-10">
        <div class="card card-nav-tabs">                    
            <div class="card-body">                      
            <h4 class="card-title">{{$data->transaction->code}}
                @if ($data->status == "0")
                <span class="ml-4"><button  class="btn btn-sm float-right btn-warning">Not Payed</button></span>
                @elseif($data->status == "1")
                <span class="ml-4"><button  class="btn btn-sm float-right btn-success">Payed</button></span>
                @else
                <span class="ml-4"><button  class="btn btn-sm float-right btn-danger">Canceled</button></span>
                @endif
                </h4>
              <p class="card-text text-secondary"><i> Owner : {{$data->user->name}}</i></p>
              <hr>
              <p>Name : {{$data->item->name}}</p>
              <p>Price : {{number_format($data->price,2,".",",")}}</p>
              <p>Quantity : {{$data->quantity}}</p>
              <p>Address : </p>                        
                <p>
                    Country : {{$data->address_country}}<br>                            
                    Province : {{$data->address_province}}<br>
                    City : {{$data->address_city}}<br>
                    Postal Code : {{$data->address_postal}}
                </p>                        
            <p>Detail : {{$data->address_detail}}</p>

              {{-- <a href="#" class="btn btn-sm btn-info">Detail</a>                       --}}
            </div>
          </div>
    </div>
</div>        
@endsection