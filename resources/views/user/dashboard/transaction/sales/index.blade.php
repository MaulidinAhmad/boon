@extends('user.dashboard.layouts.app', ['activePage' => 'sales', 'titlePage' => __('Sales')])
@section('content')
@foreach ($data as $datas)                    
        <div class="row justify-content-center" style="margin-bottom:-50px;">
            <div class="col-md-9">
                <div class="card card-nav-tabs">                    
                    <div class="card-body">
                    <h4 class="card-title mb-3">{{$datas->code}}
                        @if ($datas->transactionDetail->status == "0")
                        <span class="">                            
                        <button class="btn float-right btn-sm btn-warning">Pending</button></span>
                        @elseif($datas->transactionDetail->status == "1")
                        <span class="ml-4"><button class="btn float-right btn-sm btn-success">Send</button></span>
                        @else
                        <span class="ml-4"><button class="btn float-right btn-sm btn-danger">Canceled</button></span>
                        @endif
                        </h4>                      
                        
                          <a href="{{$datas->detail_linkusersales}}" class="btn btn-sm btn-info mt-3">Detail</a>                                                                                         
                        
                    </div>
                </div>
            </div>
        </div>
        @endforeach        
        @if (count($data) > 0)            
        <div class="row">
            <div class="col-md-8">
                {{$data->links()}}

            </div>
        </div>
                
        @endif

@endsection