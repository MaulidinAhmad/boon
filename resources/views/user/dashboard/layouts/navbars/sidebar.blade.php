<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="{{url('/')}}" class="simple-text logo-normal">
      <img src="{{asset('icon/new/icon.png')}}" alt="" style="height:64px;">
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('user.home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#laravelExample" aria-expanded="false">
          <i class="material-icons">person</i>
          <p>{{ __('Account') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="laravelExample">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('user.profile.edit') }}">
                <span class="sidebar-mini"> UP </span>
                <span class="sidebar-normal">{{ __('User profile') }} </span>
              </a>
            </li>            
          </ul>
        </div>
      </li>
      <li class="nav-item{{ $activePage == 'items' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('user.item') }}">
          <i class="material-icons">list</i>
            <p>{{ __('Items') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'sales' || $activePage == 'order') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#transactionBar" aria-expanded="false">
          <i class="material-icons">shopping_cart</i>
          <p>{{ __('Transaction') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="transactionBar">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'sales' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('user.transaction.sales') }}">
                <span class="sidebar-mini"> Sa </span>
                <span class="sidebar-normal">{{ __('Sales') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'order' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('user.transaction.order') }}">
                <span class="sidebar-mini"> Or </span>
                <span class="sidebar-normal">{{ __('Order') }} </span>
              </a>
            </li>            
          </ul>
        </div>
      </li>
    </ul>
  </div>
</div>