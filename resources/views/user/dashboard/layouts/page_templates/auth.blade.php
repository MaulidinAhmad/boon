<div class="wrapper ">
  @include('user.dashboard.layouts.navbars.sidebar')
  <div class="main-panel">
    @include('layouts.dashboard.navbars.navs.auth')
    <div class="content">
        <div class="content">
          <div class="container-fluid">
    @yield('content')
      </div>
    </div>
    </div>
    @include('layouts.dashboard.footers.auth')
  </div>
</div>