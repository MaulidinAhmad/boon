@extends('user.dashboard.layouts.app', ['activePage' => 'items', 'titlePage' => __('Add Item')])
@push('css')
    <link rel="stylesheet" href="{{url('user')}}/plugins/fileinput/css/fileinput.css">
    
@endpush
@section('content')
<div class="row">
    <div class="col-md-12">
      <form method="post" action="" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
        @csrf        

        <div class="card ">
          <div class="card-header card-header-rose card-header-icon">
            <div class="card-icon">
              <i class="material-icons">list</i>
            </div>
            <h4 class="card-title">{{ __('Add Item') }}</h4>
            <p class="card-category">{{ __('Add your product') }}</p>
          </div>
          <div class="card-body ">
            @if (session('status'))
              <div class="row">
                <div class="col-sm-12">
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span>{{ session('status') }}</span>
                  </div>
                </div>
              </div>
            @endif
            <div class="row justify-content-center">
              <label class="col-sm-2 col-form-label">{{ __('Name') }}</label>
              <div class="col-sm-8">
                <div class="form-group">
                  <input class="form-control" name="name" id="input-name" type="text"  required="true" aria-required="true"/>        
                </div>
              </div>
            </div>
            <div class="row justify-content-center">
              <label class="col-sm-2 col-form-label">{{ __('Quantity') }}</label>
              <div class="col-sm-8">
                <div class="form-group">
                  <input class="form-control" name="quantity" id="input-name" type="number"  required="true" aria-required="true"/>        
                </div>
              </div>
            </div>            
            <div class="row justify-content-center">
              <label class="col-sm-2 col-form-label">{{ __('Price') }}</label>
              <div class="col-sm-8">
                <div class="form-group">
                  <input class="form-control" name="price" id="input-name" type="number" required="true" aria-required="true"/>        
                </div>
              </div>
            </div>
            <div class="row justify-content-center">
                <label class="col-sm-2 col-form-label">{{ __('Category') }}</label>
                <div class="col-sm-8">
                    <div class="form-group">
                        {{-- <label for="exampleFormControlSelect1">Example select</label> --}}
                        <select class="form-control selectpicker" multiple name="item_category[]" data-style="btn btn-link" >
                          <option value="">-- Category --</option>
                          @foreach ($category as $category)
                              <option value="{{$category->id}}">{{$category->category_name}}</option>
                          @endforeach
                        </select>
                      </div>
                </div>
              </div>
            <div class="row justify-content-center">
                <label class="col-sm-2 col-form-label">{{ __('Country Location') }}</label>
                <div class="col-sm-8">
                    <div class="form-group">
                        {{-- <label for="exampleFormControlSelect1">Example select</label> --}}
                        <select class="form-control selectpicker" name="country_location" data-style="btn btn-link" >
                          <option value="">-- Country --</option>
                          @foreach ($country as $country)
                              <option value="{{$country->id}}">{{$country->name}}</option>
                          @endforeach
                        </select>
                      </div>
                </div>
              </div>
              <div class="row justify-content-center">
                  <label class="col-sm-2 col-form-label">{{ __('Province Location') }}</label>
                  <div class="col-sm-8">
                    <div class="form-group">
                        <select id="province-select" class="form-control selectpicker" name="province_location" data-style="btn btn-link" >
                          <option value="">-- Province --</option>
                            @foreach ($province as $province)
                                <option value="{{$province->province_id}}" idprovince="{{$province->province_id}}">{{$province->province}}</option>
                            @endforeach 
                        </select>
                    </div>
                  </div>
                </div>
                <input type="hidden" id="id_city" name="id_city">
                <div class="row justify-content-center">
                    <label  class="col-sm-2 col-form-label">{{ __('City Location') }}</label>
                    <div class="col-sm-8">
                      <div class="form-group">
                          <select id="city-select" class="form-control selectpicker" name="city_location" data-style="btn btn-link" >
                              <option value="">-- City --</option>
                            </select>
                      </div>
                    </div>
                  </div>
                  {{-- <div class="row justify-content-center">
                      <label class="col-sm-2 col-form-label">{{ __('Postal Code') }}</label>
                      <div class="col-sm-8">
                        <div class="form-group">
                          <input class="form-control" id="postal-code" name="postal_code" id="input-name" type="text" required="true" aria-required="true"/>        
                        </div>
                      </div>
                    </div> --}}
            <div class="row justify-content-center">
              <label class="col-sm-2 col-form-label">{{ __('Book Description') }}</label>
              <div class="col-sm-8">
                <div class="form-group">
                  <textarea class="form-control" rows="4" name="special_condition"></textarea>                    
                  
                </div>
              </div>
            </div>
            <div class="row justify-content-center">
              <label class="col-sm-2 col-form-label">Upload Book Picts</label>
              <div class="col-sm-8">                                  
                  <button onclick="addImage()" type="button" class="btn "><i class="glyphicon glyphicon-plus"></i> Add Pict</button>                                    
                </div>              
              </div>
                      <div class="row form-group" id="kv-avatar-more">
            </div>

          </div>
          <div class="card-footer ml-auto mr-auto">
            <button type="submit" class="btn btn-info">{{ __('Save') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection
@push('js')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="{{url('user')}}/plugins/fileinput/js/fileinput.min.js"></script>  
<script>  
  function fileinput(target, imagePreview) {
      if (!imagePreview) {
          imagePreview = "https://placehold.it/250x250";
      }
      if(target == null) {
          target = $(".fileinput");
      }
      // console.log(target);
      target.fileinput({
          showCaption: false,
          showCancel: false,
          showUpload: false,
          progressClass: "hidden",
          previewFileType: "image",
          browseClass: "btn btn-default btn-sm",
          browseLabel: "Upload",
          browseIcon: "<i class=\"icon icon-picture\"></i> ",
          removeClass: "btn btn-danger btn-sm",
          removeLabel: "Cancel",
          removeIcon: "<i class=\"icon icon-trash\"></i> ",
          initialPreview: ["<img src=" + imagePreview + " class='file-preview-image' alt=''>"],
          fileActionSettings: {
              showZoom: false
          },
          overwriteInitial: true
      });
  }
  function addImage() {
      var wrap = $("#kv-avatar-more");

      var html = '<div class="col-sm-3">' +
          '<div class="kv-avatar center-block" style="width:200px">' +
          '<input  name="gambar[]" data-preview="http://placehold.it/200x200" data-id="" multiple type="file" class="file-loading fileinput">' +
          '</div>' +
          '</div>';

      $(wrap).append(html);

      $('.fileinput').each(function(){
          $(this).fileinput({
              overwriteInitial: true,
              showClose: false,
              showCaption: false,
              showRemove: false,
              deleteExtraData: {_token:"{{csrf_token()}}"},
              browseLabel: '',
              removeLabel: '',
              browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
              removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
              removeTitle: 'Cancel or reset changes',
              initialPreview: [$(this).data('preview')],
              initialPreviewAsData: true,
              initialPreviewConfig: [{key: $(this).data('id')}],
              deleteUrl: base_url + "/admin/portofolio/delete/image",
              elErrorContainer: '.error-div',
              msgErrorClass: 'alert alert-block alert-danger',
              defaultPreviewContent: '<img src="'+ $(this).data('preview') +'" alt="Your Avatar" style="width:200px">',
              layoutTemplates: {main2: '{preview} <div class="text-center">{remove} {browse}</div>'},
              allowedFileExtensions: ["jpg", "png", "gif"]
          });
          $(this).on("filepredelete", function(jqXHR) {
//                var abort = true;
//                if (confirm("Are you sure you want to delete this image?")) {
//                    abort = false;
//                    $(this).parent().parent().parent().parent().parent().remove();
//                }
//                return abort;
              $(this).parent().parent().parent().parent().parent().remove();
          });
      });
  }
</script>
<script>
$(document).ready(function () {
    
    fileinput($('.inputfile'), "");
    });

    
</script>
<script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    
    
    $("select#province-select").change(function () { 	 
      var id = $(this).children("option:selected").attr('idprovince');	
      $.ajax({
      type: "post",
      url: "{{route('user.checkout.add.getCity')}}",
      data: {
        id:id,
      },
      dataType: "json",
      success: function (data) {	
        console.log(data)
        var provinces="<option>--City--</option>";
        for (let i = 0; i < data.length; i++) {
          provinces += '<option idcity="'+data[i].city_id+'" postal="'+data[i].postal_code+'" value="'+data[i].city_id+'">'+data[i].city_name+'</option>' ;			
        }        
        $("select#city-select").html(provinces).selectpicker('refresh');		
      }
    });	
    });
    
    $("select#city-select").change(function () { 	 
      var postal = $(this).children("option:selected").attr('postal');
      var id_city = $(this).children("option:selected").attr('idcity');	
      // $('#postal-code').val(postal);
      $("input#id_city").val(id_city);
    });
        
    
    </script>	
@endpush