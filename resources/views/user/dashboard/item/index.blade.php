@extends('user.dashboard.layouts.app', ['activePage' => 'items', 'titlePage' => __('Items')])
@section('content')
<div class="row mb-5">
  <div class="col-md-12">
    <a href="{{route('user.listing.form')}}"><button class="btn btn-info float-right">Add Item</button></a>  
  </div>
</div>
    <br>
    <div class="row">
      @foreach ($items as $item)                                                        
      <div class="col-md-4">
        <div class="card card-product">
          <div class="card-header card-header-image" data-header-animation="true" >
            <a href="">                                  
              <img class="img" src="{{$item->pict}}" style="height:250px;">
            </a>
          </div>
          <div class="card-body">
            <div class="card-actions text-center">
              <button type="button" class="btn btn-danger btn-link fix-broken-card">
                
              </button>
              <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="View">
                <i class="material-icons">art_track</i>
              </button>
              <a href="{{$item->edit_link}}">
                <button type="button" class="btn btn-success btn-link" rel="tooltip" data-placement="bottom" title="Edit">
                  <i class="material-icons">edit</i>
                </button>
              </a>
              <a href="{{route('user.listing.delete',['id'=>$item->id])}}">                                  
                <button type="button" class="btn btn-danger btn-link" rel="tooltip" data-placement="bottom" title="Remove">
                  <i class="material-icons">close</i>
                </button>
              </a>
            </div>
            <h4 class="card-title">
              <a href="#pablo">{{$item->name}}</a>
            </h4>
            <h5 class="card-title">              
                <a href="#pablo">Category :                                     
                  @if (count($item->item_category) > 0)
                    @php
                        $a=0;
                        $lastloop=count($item->item_category);                        
                    @endphp
                    @for ($i = 0; $i <  count($item->item_category); $i++)                      
                    @if(++$a == $lastloop)
                    {{ucfirst(strtolower($item->item_category[$i]['category'][0]['category_name']))}}
                    @else
                    {{ucfirst(strtolower($item->item_category[$i]['category'][0]['category_name']))}},
                    @endif
                    @endfor
                  @else
                  {{ucfirst(strtolower($item->item_category[0]['category'][0]['category_name']))}}
                  @endif
                </a>
              </h5>
            <div class="card-description">
              {{$item->special_condition}}
            </div>
            <div class="center">
                @if ($item->is_published == 1)
                <button type="button" rel="tooltip" class="btn btn-sm btn-success">
                  Active
                </button>
              @elseif($item->is_published == 0)
                <button type="button" rel="tooltip" class="btn btn-sm btn-danger">
                    Not Active
                </button>
              @endif
            </div>
          </div>
          <div class="card-footer">
            <div class="price">
              <h4>Rp. {{number_format($item->price,2,",",".")}}</h4>
            </div>
            <div class="stats">              
              <p class="card-category"><i class="material-icons">place</i> {{$item->location->country->name.", ".$item->location->province->province}}</p>
            </div>
          </div>
        </div>
      </div>        
    @endforeach                                        
  </div>
  <div class="row">
    <div class="col-md-12">
      {{$items->links()}}
    </div>
  </div>
@endsection