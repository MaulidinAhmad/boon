@extends('user.layouts.login.app')
@push('css')	
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('user/login')}}/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{asset('user/login')}}/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('user/login')}}/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('user/login')}}/css/util.css">
	<link rel="stylesheet" type="text/css" href="{{asset('user/login')}}/css/main.css">
@endpush
@section('content')
	<div class="limiter">
		<a class="navbar-brand" href="{{ url('/') }}"><img src="{{asset('icon/new/icon.png')}}" style="height:48px;" alt=""></a>
		<div class="container-login100" style="background-width:100%;">		
			
			<div class="wrap-login100" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);background-color:#45a7d1">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="{{asset('user/login')}}/images/img-01.png" alt="IMG">
				</div>

                <form class="login100-form validate-form" method="POST" action="">
                {{ csrf_field() }}
					<span class="login100-form-title">
						Member Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="email" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>
					@error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong style="padding-left:24px;padding-bottom:8px;margin-top:0px;">{{ $message }}</strong>
                    </span>
                    @enderror

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					@error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong style="padding-left:24px;padding-bottom:8px;margin-top:0px;">{{ $message }}</strong>
                    </span>
                    @enderror
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							Forgot
						</span>
						<a class="txt2" href="#">
							Username / Password?
						</a>
					</div>

					<div class="text-center p-t-136">
                    <a class="txt2" href="{{url('user/auth/register')}}">
							Create your Account
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>

</section><!--/form-->
@endsection
@push('js')
{{-- <script src="{{asset('user/login')}}/vendor/jquery/jquery-3.2.1.min.js"></script> --}}
<!--===============================================================================================-->
	<script src="{{asset('user/login')}}/vendor/bootstrap/js/popper.js"></script>
	{{-- <script src="{{asset('user/login')}}/vendor/bootstrap/js/bootstrap.min.js"></script> --}}
<!--===============================================================================================-->
	<script src="{{asset('user/login')}}/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="{{asset('user/login')}}/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="{{asset('user/login')}}/js/main.js"></script>
@endpush
