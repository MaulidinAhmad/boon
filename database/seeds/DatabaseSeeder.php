<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Steevenz\Rajaongkir;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$now=Carbon::now()->toDateTimeString();
        // DB::table('item_categories')->delete();
      $rajaongkir=new Rajaongkir;
      $datas=$rajaongkir->getCities();  
		// $datas = array(
		// 	array('category_name' => 'TECHNOLOGY', 'created_at'=>$now,'updated_at'=>$now),
		// 	array('category_name' => 'FILOSOFI', 'created_at'=>$now,'updated_at'=>$now),
		// 	array('category_name' => 'FOOD', 'created_at'=>$now,'updated_at'=>$now),
		// 	array('category_name' => 'HEALTH', 'created_at'=>$now,'updated_at'=>$now),
		// 	array('category_name' => 'LIFE STYLE', 'created_at'=>$now,'updated_at'=>$now),
		// );
		DB::table('cities')->insert($datas);
    }
}
