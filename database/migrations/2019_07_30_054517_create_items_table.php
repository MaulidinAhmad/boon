<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // if(Schema::hasTable('items')) return;                        
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->string('name')->nullable();                    
            $table->integer('quantity');
            $table->integer('price');            
            $table->string('product_type'); // Store name of product type
            $table->string('special_condition')->nullable();                                                            
            $table->enum('is_published', [0, 1, 2])->default(0);
            $table->enum('is_seller_modified', [0, 1, 2])->default(0);
            $table->string('is_seller_modified_notif')->default("{status:0;message:''}");
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
