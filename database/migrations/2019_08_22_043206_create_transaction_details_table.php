<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('transaction_details')) return;                                                
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_transaction')->unsigned();
            $table->smallInteger('id_item');
            $table->smallInteger('id_owner');
            $table->smallInteger('price');
            $table->smallInteger('quantity');
            $table->text('owner_message')->nullable();
            $table->smallInteger('is_owner_readed')->default(0);
            $table->smallInteger('is_owner_replied')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
