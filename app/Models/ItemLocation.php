<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemLocation extends Model
{
    public function item(){    
            return $this->belongsTo(Item::Class, 'id', 'id_item');
    }
    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }
    public function province()
    {
        return $this->hasOne(Province::class, 'id', 'province_id');
    }
    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }
}
