<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'id_transaction', 'id');
    }    

    public function item()
    {
        return $this->hasOne(Item::class, 'id', 'id_item');
    }

    public function user()
    {
        return $this->hasOne(User::class, "id", "id_owner");
    }
    public function getAddressCountryAttribute(){
        $address=json_decode(json_decode(json_encode($this->send_address),true),true);
        foreach ($address as $key => $value) {
            $address=$value['country'];            
        }        
        return $address;
    }
    public function getAddressCityAttribute(){
        $address=json_decode(json_decode(json_encode($this->send_address),true),true);
        foreach ($address as $key => $value) {
            $id_city=$value['city'];            
        }
        $city=City::findOrFail($id_city);
        return $city->city_name;
    }
    public function getAddressProvinceAttribute(){
        $address=json_decode(json_decode(json_encode($this->send_address),true),true);
        foreach ($address as $key => $value) {
            $id_province=$value['province'];            
        }
        $province=Province::findOrFail($id_province);
        return $province->province;
    }
    public function getAddressPostalAttribute(){
        $address=json_decode(json_decode(json_encode($this->send_address),true),true);
        foreach ($address as $key => $value) {
            $postal_code=$value['postal_code'];            
        }       
        return $postal_code;
    }
    public function getAddressPhoneAttribute(){
        $address=json_decode(json_decode(json_encode($this->send_address),true),true);
        foreach ($address as $key => $value) {
            $number_phone=$value['number_phone'];            
        }       
        return $number_phone;
    }
    public function getAddressDetailAttribute(){
        $address=json_decode(json_decode(json_encode($this->send_address),true),true);
        foreach ($address as $key => $value) {
            $detail=$value['detail'];            
        }       
        return $detail;
    }
    public function getPayedLinkAttribute(){
        return route('admin.transaction.setPayed',["id"=>$this->id]);
    }
    public function getNotpayedLinkAttribute(){
        return route('admin.transaction.setNotPayed',["id"=>$this->id]);
    }
    public function getCanceledLinkAttribute(){
        return route('admin.transaction.setCanceled',["id"=>$this->id]);
    }
}
