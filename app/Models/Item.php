<?php
use Carbon\Carbon;
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function photos(){        
        return $this->hasMany(ItemPhoto::class, "id_item", "id");
    }

    public function location(){        
        return $this->hasOne(ItemLocation::class, "id_item", "id");
    }

    public function item_category()
    {
        // return $this->belongsTo(ItemCategory::class,"id_category","id");
        return $this->hasMany(ItemCategory::class,"id_item","id");
    }

    public function getEditLinkAttribute(){
        return route('user.listing.edit',['id' => $this->id]);
    }

    public function getEditLinkadminAttribute(){
        return route('admin.listing.edit',['id' => $this->id]);
    }

    public function getPictAttribute()
    {
        if ($this->photos()->count() < 1){
            return url('user_assets/images/default.png');
        } else {
            $photo = $this->photos()->first()->pict;
            return url('img/item/'.$photo);
        }
    }

    public function user()
    {
        return $this->hasOne(User::class, "id", "id_user");
    }

    public function admin()
    {
        return $this->hasOne(Admin::class, "id", "id_user");
    }

    public function transaction()
    {
        return $this->belongsTo(Item::class, 'id_item', 'id');
    }

    
}
