<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function item()
    {
        return $this->hasOne(Item::class, 'id', 'id_item');
    }

    public function buyer()
    {
        return $this->hasOne(User::class, 'id', 'id_buyer');
    }

    public function transactionDetail()
    {
        return $this->hasOne(TransactionDetail::class, "id_transaction", "id");
    }
    public function getDetailLinkusersalesAttribute(){
        return route('user.transaction.sales.detail',["id"=>$this->id]);
    }
    public function getDetailLinkuserorderAttribute(){
        return route('user.transaction.order.detail',["id"=>$this->id]);
    }
    public function getDetailLinkAttribute(){
        return route('admin.transaction.detail',["id"=>$this->id]);
    }
    public function getPayedLinkAttribute(){
        return route('admin.transaction.setPayed',["id"=>$this->transactionDetail->id]);
    }
    public function getNotpayedLinkAttribute(){
        return route('admin.transaction.setNotPayed',["id"=>$this->transactionDetail->id]);
    }
    public function getCanceledLinkAttribute(){
        return route('admin.transaction.setCanceled',["id"=>$this->transactionDetail->id]);
    }    
    //
}
