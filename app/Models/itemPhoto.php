<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemPhoto extends Model
{
    public function item(){
        return $this->belongsTo(Item::class, 'id','id_item');
    }

    public function getUrlAttribute(){
        return url('img/item/'.$this->pict);
    }
}
