<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    // protected $table='item_categories';
    public function item()
    {
        return $this->belongsTo(Item::class,"id","id_item");
    }    
    public function category()
    {
        return $this->hasMany(Category::class, 'id', 'id_category');
    }
}
