<?php

namespace App\Http\Controllers\Admin\Item;


use Alert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\ItemPhoto;
use App\Models\ItemLocation;
use App\Models\ItemCategory;
use App\Models\Category;
use App\Models\Province;
use App\Models\City;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\Country;
use Steevenz\Rajaongkir;
use Illuminate\Database\Eloquent\Builder;

class ItemController extends Controller
{
    public function index(){
        $items=Item::with('item_category.category')->paginate(9);
        $data['items']=$items;
        return view('admin.item.index',$data);
    }

    public function showAddForm(){
        $data['country']=Country::get();             
        $data['province']=Province::get();  
        $data['category']=Category::get();
        return view('admin.item.listing.stock',$data);
    }

    public function addForm(Request $request){        
        $current_date_time = Carbon::now()->toDateTimeString();
        $item = new Item;
        $item->id_user=Auth::user()->id;
        $item->name=$request->name;
        $item->quantity=$request->quantity;
        $item->price=$request->price;        
        $item->special_condition=$request->special_condition;     
        $item->created_at=$current_date_time;
        $item->updated_at=$current_date_time;
        $item->save();

        for ($i=0; $i <count($request->item_category) ; $i++) { 
        $item_category=new ItemCategory;
        $item_category->id_item=$item->id;
        $item_category->id_category=$request->item_category[$i];
        $item_category->created_at=$current_date_time;
        $item_category->updated_at=$current_date_time;
        $item_category->save();
        }        
        
        $location=new ItemLocation;
        $location->id_item=$item->id;
        $location->country_id=$request->country_location;
        $location->province_id=$request->province_location;
        $location->city_id=$request->city_location;                
        $location->created_at=$current_date_time;
        $location->updated_at=$current_date_time;
        $location->save();
        
        // $images=$request->file('gambar');
        if($request->hasFile('gambar'))
        {
            foreach ($request->file('gambar') as $gambar) {
                $file = $gambar;
                $fileName = 'ITM_' . str_random(6) . '_' . snake_case($file->getClientOriginalName());
                $gambar->move("img/item/", $fileName);
                $photo = new ItemPhoto;
                $photo->id_item = $item->id;
                $photo->pict = $fileName;
                $photo->save();
            }
        }

        Alert::success('Success add Item !', 'Success');
        return redirect(route('admin.item'));
    }
    public function editItem($id){
        $data=Item::where('id',$id)->with('item_category.category')->first();
        $id_category=[];
        for ($a=0;$a<count($data['item_category']);$a++) {
            $id_category[]=$data['item_category'][$a]['category'][0]['id'];
        }
        $data['id_category']=$id_category;
        $data['country']=Country::get();
        $data['items']=$data;
        $data['category']=Category::get();              
        $data['province']=Province::get(); 
        $province_id=Item::where('id',$id)->with('location.province')->first();
        $province_id=$province_id['location']['province']['id'];
        $data['city']=City::where('province_id',$province_id)->get(); 
        $data['id']=$id;
        return view('admin.item.listing.edit',$data);
    }
    
    public function updateItem(Request $request){        
        $current_date_time = Carbon::now()->toDateTimeString();
        $id=$request->id;
        $item=Item::where('id',$id)->first();
        $id_user=$item->id_user;
        $item = Item::find($id);
        $item->id_user=$id_user;
        $item->name=$request->name;
        $item->quantity=$request->quantity;
        $item->price=$request->price;
        $item->special_condition=$request->special_condition;
        $item->updated_at=$current_date_time;
        $item->save();


        ItemCategory::where('id_item',$item->id)->delete();
        for($s=0;$s<count($request->item_category); $s++){        
        $item_category=new ItemCategory;        
        $item_category->id_item=$item->id;
        $item_category->id_category=$request->item_category[$s];
        $item_category->created_at=$current_date_time;
        $item_category->updated_at=$current_date_time;
        $item_category->save();
        }
        
        $location=ItemLocation::where('id_item',$item->id)->first();
        $location->id_item=$item->id;
        $location->country_id=$request->country_location;
        $location->province_id=$request->province_location;
        $location->city_id=$request->city_location;                
        $location->created_at=$current_date_time;
        $location->updated_at=$current_date_time;
        $location->save();
                
        if($request->hasFile('gambar'))
        {            
            ItemPhoto::where('id_item',$item->id)->delete();
            foreach ($request->file('gambar') as $gambar) {
                $file = $gambar;
                $fileName = 'ITM_' . str_random(6) . '_' . snake_case($file->getClientOriginalName());
                $gambar->move("img/item/", $fileName);
                $photo = new ItemPhoto;
                $photo->id_item = $item->id;
                $photo->pict = $fileName;
                $photo->save();
            }
        }

        Alert::success('Success Edit Item !', 'Success');
        return redirect(route('admin.item'));
    }

    public function deleteItem($id){
        $photo=ItemPhoto::where('id_item',$id)->first();
        
        if ($photo != null) {        
            if(File::exists(public_path().'/img/item/'.$photo->pict)) {
                File::delete(public_path().'/img/item/'.$photo->pict);
            }
        }
        Item::find($id)->delete();
        Alert::success('Success Delete Item !', 'Success');
        return redirect(route('admin.item')); 
    }

    public function activateItem($id){

        $item=Item::findOrFail($id);
        $item->is_published='1';
        $item->save();

        Alert::success('Success Activate Item !', 'Success');
        return redirect(route('admin.item'));
    }
    public function deactivateItem($id){
        $item=Item::findOrFail($id);
        $item->is_published='0';
        $item->save();

        return redirect(route('admin.item'));
    }
    public function stockPhotoDelete($id){
        $photo = ItemPhoto::findOrFail($id);
        \File::delete(public_path().'/img/item/'.$photo->pict);
        $photo->delete();
        return \Response::json(['resp' => true], 200);    
    }
    public function getCity(Request $request){
        $id_province = $request->id;
        $cities = City::where('province_id',$id_province)->get();    
        return response()->json($cities);
        
    }
}
