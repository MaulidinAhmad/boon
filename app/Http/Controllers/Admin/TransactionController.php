<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Alert;

class TransactionController extends Controller
{
    public function index(){
        $data['data']=Transaction::groupBy('code')->orderBy('created_at','desc')->get();
        return view('admin.transaction.index',$data);
    }

    public function detail($id){
        $data['data']=TransactionDetail::where('id_transaction',$id)->first();
        return view('admin.transaction.detail',$data);
    }

    public function setPayed($id){        
        $transaction_detail=TransactionDetail::where('id',$id)->first();
        $transaction_detail->status="1";
        $transaction_detail->save();                
        Alert::success('Status has been changed','Success');
        return redirect()->back();
    }
    public function setNotPayed($id){        
        $transaction_detail=TransactionDetail::where('id',$id)->first();
        $transaction_detail->status="0";
        $transaction_detail->save();        
        Alert::success('Status has been changed','Success');
        return redirect()->back();
    }
    public function setCanceled($id){        
        $transaction_detail=TransactionDetail::where('id',$id)->first();
        $transaction_detail->status="2";
        $transaction_detail->save();        
        Alert::success('Status has been changed','Success');
        return redirect()->back();
       
    }
}
