<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
     /**
     * Send login form admin
     *
     * @return mixed
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }

//     public function login(\Illuminate\Http\Request $request) {
//       $this->validateLogin($request);

//       // If the class is using the ThrottlesLogins trait, we can automatically throttle
//       // the login attempts for this application. We'll key this by the username and
//       // the IP address of the client making these requests into this application.
//       if ($this->hasTooManyLoginAttempts($request)) {
//         $this->fireLockoutEvent($request);
//         return $this->sendLockoutResponse($request);
//       }

//       // This section is the only change
//       if ($this->guard()->validate($this->credentials($request))) {
//         $user = $this->guard()->getLastAttempted();

//         // Make sure the user is active
//         if ($user->status == 1 && $this->attemptLogin($request)) {
//           // Send the normal successful login response
//           return $this->sendLoginResponse($request);
//         } else {
//           // Increment the failed login attempts and redirect back to the
//           // login form with an error message.
//           $this->incrementLoginAttempts($request);
//           return redirect()
//           ->back()
//           ->withInput($request->only($this->username(), 'remember'))
//           ->with('alert', 'Your account has been disabled! Please contact superadmin.');
//         }
//       }

//     // If the login attempt was unsuccessful we will increment the number of attempts
//     // to login and redirect the user back to the login form. Of course, when this
//     // user surpasses their maximum number of attempts they will get locked out.
//     $this->incrementLoginAttempts($request);

//     return $this->sendFailedLoginResponse($request);
// }

    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

}
