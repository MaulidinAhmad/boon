<?php

namespace App\Http\Controllers\User;

use Alert;
use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index(){
        $data['data']=Transaction::where('id_buyer','=',Auth::user()->id)->groupBy('code')->orderBy('created_at','desc')->paginate(9);        
        return view('user.dashboard.transaction.order.index',$data);
    }

    public function detail($id){
        $data['data']=TransactionDetail::where('id_transaction',$id)->first();
        return view('user.dashboard.transaction.detail',$data);
    }

    public function showAddForm($id){
        if (!isset(Auth::user()->id)) {
            Alert::error('Please Login First','Error');
            return redirect('user/auth/login');
        }
            $data['id']=$id;
            return view('user.transaction.addForm');
    }
}
