<?php

namespace App\Http\Controllers\User;

use App\Models\Item;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SalesController extends Controller
{
    public function index(){
        $data['data']=Transaction::whereHas('transactionDetail',function($query){
        $query->where('id_owner',Auth::user()->id);
        }        
        )->where('id_buyer','!=',Auth::user()->id)->groupBy('code')->orderBy('created_at','desc')->paginate(9); 
        return view('user.dashboard.transaction.sales.index',$data);
    }
    public function detail($id){
        $data['data']=TransactionDetail::where('id_transaction',$id)->first();
        return view('user.dashboard.transaction.detail',$data);
    }
}
