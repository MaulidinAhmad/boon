<?php

namespace App\Http\Controllers\User\Auth;

// use App\Models\Compare;
// use App\Models\ResultSaved;
// use App\Models\ItemSaved;
// use App\Models\Enquiry;
// use App\Models\Cart as Session_Cart;
// use Alert;
// use Cart;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Alert;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    // public function restoreItem($jns, $mdl){
    //     if(Cart::instance($jns)->count() > 0){
    //         foreach (Cart::instance($jns)->content() as $item) {
    //             switch ($mdl) {
    //                 case 1:
    //                     $session_item = new Compare;
    //                     break;

    //                 case 2:
    //                     $session_item = new Session_Cart;
    //                     $qty = 1;
    //                     break;

    //                 case 3:
    //                     $session_item = new ResultSaved;
    //                     $qty = 1;
    //                     break;

    //                 case 4:
    //                     $session_item = new ItemSaved;
    //                     $qty = 1;
    //                     break;
    //             }

    //             $session_item->rowId = $item->rowId;
    //             $session_item->id_product = $item->id;
    //             $session_item->name_product = $item->name;
    //             if(isset($qty)){
    //                 $session_item->qty = $item->qty;
    //             }
    //             $session_item->options = $item->options;
    //             $session_item->price = $item->price;
    //             $session_item->id_user = Auth::user()->id;
    //             $session_item->save();
    //             Cart::instance($jns)->remove($item->rowId);

    //         }
    //     }
    // }

    // private function _update_enquiry(){
    //     if(count(Cart::instance('enquiry')->content()) < 1) return;

    //     foreach (Cart::instance('enquiry')->content() as $item) {
    //         $enq = Enquiry::find($item->id);
    //         if(!$enq) continue;
            
    //         $enq->id_user = Auth::user()->id;
    //         $enq->save();

    //     }
    //     Cart::instance('enquiry')->destroy();
    // }

    // /**
    //  * The user has been authenticated.
    //  */
    // public function authenticated(Request $request, $user){
    //     $this->_update_enquiry();
    // }

    public function logout(Request $request){
        // $this->restoreItem('compare', 1);
        // $this->restoreItem('cart', 2);
        // $this->restoreItem('saved_filter', 3);
        // $this->restoreItem('saved_items', 4);

        Auth::logout();
        return redirect('/');
    }


    /**
     * Send login form admin
     *
     * @return mixed
     */
    protected function guard()
    {
        return Auth::guard('web');
    }

    public function showLoginForm()
    {
        return view('user.auth.login');
    }

    protected function authenticated(Request $request, $user)
    {
       Alert::success('Login Success !!','Success');
       return redirect()->intended($this->redirectPath());
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|exists:users',
            'password' => 'required',
        ], [
            $this->username() . '.exists' => 'The selected email is invalid or the account has been disabled (not active).'
        ]);
    }
}
