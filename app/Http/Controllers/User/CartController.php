<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cart;
use Alert;
use App\Models\Item;
class CartController extends Controller{

    public function index(){
        if (!isset(Auth::user()->id)) {
            Alert::Error('Login First To Add','Error');
            return redirect('user/auth/login');
        }
        \Cart::session(Auth::user()->id);           
        return view('user.cart.index');
    }

    public function add(Request $request){
        $item=Item::findOrFail($request->id);                      
        if (!isset(Auth::user()->id)) {
            Alert::Error('Login First To Add','Error');
            return redirect('user/auth/login');
        }
        if ($item['id_user'] == Auth::user()->id) {
            Alert::error("You can't buy your item!!","Error");
            return redirect()->back();
        }
        \Cart::session(Auth::user()->id);        
        Cart::add(
            array(
                'id'=>$item->id,                
                'name'=>$item->name,
                'price'=>$item->price,
                'quantity'=>1,
            )
        );

        Alert::success('Item added to cart!', 'success');

        return redirect()->back();
    }

    public function delete($id){
        \Cart::session(Auth::user()->id);              
        Cart::remove($id);
        Alert::success('Success Delete Cart !!!','Success');
        return redirect()->back();
    }

    public function incQuantity($id){
        $item=Item::findOrFail($id);                      
        if (!isset(Auth::user()->id)) {
            Alert::Error('Login First To Add','Error');
            return redirect('user/auth/login');
        }
        if ($item['id_user'] == Auth::user()->id) {
            Alert::error("You can't buy your item!!","Error");
            return redirect()->back();
        }
        \Cart::session(Auth::user()->id);        
        Cart::update($id,
            array(                
                'quantity'=>+1,
            )
        );        
        return redirect()->back();
    }

    public function decQuantity($id){       
        $item=Item::findOrFail($id);          
        if (!isset(Auth::user()->id)) {
            Alert::Error('Login First To Add','Error');
            return redirect('user/auth/login');
        }
        if ($item['id_user'] == Auth::user()->id) {
            Alert::error("You can't buy your item!!","Error");
            return redirect()->back();
        }
        \Cart::session(Auth::user()->id);        
        Cart::update($id,
            array(                
                'quantity'=>-1,
            )
        );        
        return redirect()->back();
    }

}

