<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Item;
Use Alert;
use Steevenz\Rajaongkir;
use App\Models\Country;
use App\Models\Transaction;
use App\Models\Province;
use App\Models\City;
use App\Models\Admin;
use App\Models\TransactionDetail;
use Carbon\Carbon;
use Validator;
use PDF;
use Cart;

class CheckoutController extends Controller
{
    
    public function index($id){    
        if (!isset(Auth::user()->id)) {
            Alert::error('Login To Buy','Error');
            return redirect('user/auth/login');
        }
        parse_str($id,$output);        
        if (is_array($output["id"])==false) {
            $id=$output["id"];
            $data['items']=Item::where('id',$id)->get();                             
            if ($data['items'][0]['id_user'] == Auth::user()->id) {
                Alert::error("You can't buy your item!!","Error");
                return redirect()->back();
            }
        }else{
            $id_in_array=array();
            for ($i=0; $i < count($output["id"]) ; $i++) { 
                $id_in_array[]=$output["id"][$i][0]."";
                $data['items']=Item::whereIn('id',$id_in_array)->get();                                                              
            }
        }                
        \Cart::session(Auth::user()->id);  
                                
        $data['province']=Province::get();
        
        $data['country']=Country::get();
        $data['id']=$id;
        // dd($cities);        
        return view('user.checkout.index',$data);
    }

    public function getOngkir(Request $request){
        $id_item=$request->id_item;
        $id_city=$request->city_id;        
        $item=Item::findOrFail($id_item);
        $id_city_from=$item->location->city->id;        
        $rajaongkir = new Rajaongkir();               
        $supportedCouriers = $rajaongkir->getSupportedCouriers();
        // dd($supportedCouriers);
        $data[0]['cost'] = $rajaongkir->getCost(['city' => $id_city_from], ['city' => $id_city], 200, 'pos');
        $data[1]['cost'] = $rajaongkir->getCost(['city' => $id_city_from], ['city' => $id_city], 200, 'jne');        
        $data[2]['cost'] = $rajaongkir->getCost(['city' => $id_city_from], ['city' => $id_city], 200, 'tiki');        
        return response()->json($data);
    }

    public function getCity(Request $request){
        $id_province = $request->id;
        $cities = City::where('province_id',$id_province)->get();    
        return response()->json($cities);
        
    }

    public function checkouted(Request $request){
        // $validator = Validator::make($request->all(), [
        //     'a' => 'required'
        // ]);
        // if ($validator->fails()) {
        //     return redirect()->back()->withErrors($validator);                        
        // }
        $current_date_time = Carbon::now()->toDateTimeString();
        $item=Item::findOrFail($request->id_item);
        $address=[];
        $address[]=[
            'country'=>$request->country_location,
            'province'=>$request->province_location,
            'city'=>$request->city_location,
            'postal_code'=>$request->postal_code,
            'number_phone'=>$request->number_phone,
            'detail'=>$request->description
        ];        

        $transaction=new Transaction;
        $transaction->code="TRS_".$request->id_item."_BOOK_".date("Y")."_".date("m")."_".date("d")."_".date("s");    
        $transaction->id_buyer=Auth::user()->id;
        $transaction->total=$request->total_checkout;
        $transaction->created_at=$current_date_time;
        $transaction->updated_at=$current_date_time;
        $transaction->save();

        $transaction_detail=new TransactionDetail;
        $transaction_detail->id_transaction=$transaction->id;
        $transaction_detail->id_item=$request->id_item;
        $transaction_detail->id_owner=$item->id_user;
        $transaction_detail->send_address=json_encode($address);
        $transaction_detail->price=$item->price;
        $transaction_detail->quantity=$request->quantity;
        $transaction_detail->created_at=$current_date_time;
        $transaction_detail->updated_at=$current_date_time;
        $transaction_detail->save();

        $data['id']=$transaction->id;
        $data['rekening']=Admin::select('rekening')->get();
        return view('user.checkout.payment',$data);
    }

    public function invoice($id){
        $data['data']=TransactionDetail::where('id_transaction',$id)->first();        
        return view('user.checkout.invoice',$data);
        // $pdf = PDF::loadView('user.checkout.invoice', $data);
        // return $pdf->download('laporan_sembako_'.date('Y-m-d_H-i-s').'.pdf');
    }

}
