<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {        
        $data['items']=Item::where('is_published','1')->limit(3)->get();        
            return view('home',$data);                
        
    }

    public function book()
    {        
        $data['items']=Item::where('is_published','1')->paginate(9);        
            return view('book',$data);                
        
    }

    public function itemDetail($id){
        $item=Item::findOrFail($id);                 
        $data['item']=$item;
        return view('itemdetail',$data);
    }

    public function categories($id,$name){
        $data['items']=Item::whereHas('item_category',function($query) use ($id){
            $query->where('id_category',$id);
        })->paginate(9);
        return view('itemcategories',$data);
    }
}
