<?php

namespace App\Providers;

use App\Models\Setting;
use Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Blade::directive('isedit', function($type) {
            return "<?php if($type=='edit'): ?>";
        });
        Blade::directive('endisedit', function($type) {
            return "<?php endif; ?>";
        });

        view()->share('website_setting', json_decode(Setting::where('name', 'website')->firstOrFail()['content'])[0]);
    }
}
