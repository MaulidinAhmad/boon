-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 05 Sep 2019 pada 04.34
-- Versi server: 10.4.7-MariaDB
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bon`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `rekening` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `rekening`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin Adminsss', 'admin@material.com', '2019-07-25 05:50:47', '2657893768', '$2y$10$KAAGOxJ/CcN7/JxMI/UgHeSMjzZPWzJJmCa.1XCUg4xyS5SAubE2C', NULL, '2019-07-25 05:50:47', '2019-08-07 21:47:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cart_storage`
--

CREATE TABLE `cart_storage` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cart_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `cart_storage`
--

INSERT INTO `cart_storage` (`id`, `cart_data`, `created_at`, `updated_at`) VALUES
('15_cart_items', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:2:{i:75;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:13:\"App\\DBStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:6:{s:2:\"id\";i:75;s:4:\"name\";s:6:\"sadsad\";s:5:\"price\";i:233232;s:8:\"quantity\";i:3;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}}}i:76;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:13:\"App\\DBStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:6:{s:2:\"id\";i:76;s:4:\"name\";s:6:\"Bookk3\";s:5:\"price\";i:20000;s:8:\"quantity\";i:4;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}}}}}', '2019-09-02 19:12:26', '2019-09-04 18:44:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'EDUCATION', '2019-08-29 17:00:00', '2019-08-29 17:00:00'),
(2, 'SPORT', '2019-08-29 17:00:00', '2019-08-29 17:00:00'),
(3, 'TECHNOLOGY', '2019-08-30 07:12:20', '2019-08-30 07:12:20'),
(4, 'FILOSOFI', '2019-08-30 07:12:20', '2019-08-30 07:12:20'),
(5, 'FOOD', '2019-08-30 07:12:20', '2019-08-30 07:12:20'),
(6, 'HEALTH', '2019-08-30 07:12:20', '2019-08-30 07:12:20'),
(7, 'LIFE STYLE', '2019-08-30 07:12:20', '2019-08-30 07:12:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cities`
--

CREATE TABLE `cities` (
  `id` int(10) NOT NULL,
  `city_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `cities`
--

INSERT INTO `cities` (`id`, `city_id`, `province_id`, `province`, `type`, `city_name`, `postal_code`) VALUES
(1, 1, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Barat', '23681'),
(2, 2, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Barat Daya', '23764'),
(3, 3, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Besar', '23951'),
(4, 4, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Jaya', '23654'),
(5, 5, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Selatan', '23719'),
(6, 6, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Singkil', '24785'),
(7, 7, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Tamiang', '24476'),
(8, 8, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Tengah', '24511'),
(9, 9, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Tenggara', '24611'),
(10, 10, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Timur', '24454'),
(11, 11, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Utara', '24382'),
(12, 12, 32, 'Sumatera Barat', 'Kabupaten', 'Agam', '26411'),
(13, 13, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Alor', '85811'),
(14, 14, 19, 'Maluku', 'Kota', 'Ambon', '97222'),
(15, 15, 34, 'Sumatera Utara', 'Kabupaten', 'Asahan', '21214'),
(16, 16, 24, 'Papua', 'Kabupaten', 'Asmat', '99777'),
(17, 17, 1, 'Bali', 'Kabupaten', 'Badung', '80351'),
(18, 18, 13, 'Kalimantan Selatan', 'Kabupaten', 'Balangan', '71611'),
(19, 19, 15, 'Kalimantan Timur', 'Kota', 'Balikpapan', '76111'),
(20, 20, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Banda Aceh', '23238'),
(21, 21, 18, 'Lampung', 'Kota', 'Bandar Lampung', '35139'),
(22, 22, 9, 'Jawa Barat', 'Kabupaten', 'Bandung', '40311'),
(23, 23, 9, 'Jawa Barat', 'Kota', 'Bandung', '40111'),
(24, 24, 9, 'Jawa Barat', 'Kabupaten', 'Bandung Barat', '40721'),
(25, 25, 29, 'Sulawesi Tengah', 'Kabupaten', 'Banggai', '94711'),
(26, 26, 29, 'Sulawesi Tengah', 'Kabupaten', 'Banggai Kepulauan', '94881'),
(27, 27, 2, 'Bangka Belitung', 'Kabupaten', 'Bangka', '33212'),
(28, 28, 2, 'Bangka Belitung', 'Kabupaten', 'Bangka Barat', '33315'),
(29, 29, 2, 'Bangka Belitung', 'Kabupaten', 'Bangka Selatan', '33719'),
(30, 30, 2, 'Bangka Belitung', 'Kabupaten', 'Bangka Tengah', '33613'),
(31, 31, 11, 'Jawa Timur', 'Kabupaten', 'Bangkalan', '69118'),
(32, 32, 1, 'Bali', 'Kabupaten', 'Bangli', '80619'),
(33, 33, 13, 'Kalimantan Selatan', 'Kabupaten', 'Banjar', '70619'),
(34, 34, 9, 'Jawa Barat', 'Kota', 'Banjar', '46311'),
(35, 35, 13, 'Kalimantan Selatan', 'Kota', 'Banjarbaru', '70712'),
(36, 36, 13, 'Kalimantan Selatan', 'Kota', 'Banjarmasin', '70117'),
(37, 37, 10, 'Jawa Tengah', 'Kabupaten', 'Banjarnegara', '53419'),
(38, 38, 28, 'Sulawesi Selatan', 'Kabupaten', 'Bantaeng', '92411'),
(39, 39, 5, 'DI Yogyakarta', 'Kabupaten', 'Bantul', '55715'),
(40, 40, 33, 'Sumatera Selatan', 'Kabupaten', 'Banyuasin', '30911'),
(41, 41, 10, 'Jawa Tengah', 'Kabupaten', 'Banyumas', '53114'),
(42, 42, 11, 'Jawa Timur', 'Kabupaten', 'Banyuwangi', '68416'),
(43, 43, 13, 'Kalimantan Selatan', 'Kabupaten', 'Barito Kuala', '70511'),
(44, 44, 14, 'Kalimantan Tengah', 'Kabupaten', 'Barito Selatan', '73711'),
(45, 45, 14, 'Kalimantan Tengah', 'Kabupaten', 'Barito Timur', '73671'),
(46, 46, 14, 'Kalimantan Tengah', 'Kabupaten', 'Barito Utara', '73881'),
(47, 47, 28, 'Sulawesi Selatan', 'Kabupaten', 'Barru', '90719'),
(48, 48, 17, 'Kepulauan Riau', 'Kota', 'Batam', '29413'),
(49, 49, 10, 'Jawa Tengah', 'Kabupaten', 'Batang', '51211'),
(50, 50, 8, 'Jambi', 'Kabupaten', 'Batang Hari', '36613'),
(51, 51, 11, 'Jawa Timur', 'Kota', 'Batu', '65311'),
(52, 52, 34, 'Sumatera Utara', 'Kabupaten', 'Batu Bara', '21655'),
(53, 53, 30, 'Sulawesi Tenggara', 'Kota', 'Bau-Bau', '93719'),
(54, 54, 9, 'Jawa Barat', 'Kabupaten', 'Bekasi', '17837'),
(55, 55, 9, 'Jawa Barat', 'Kota', 'Bekasi', '17121'),
(56, 56, 2, 'Bangka Belitung', 'Kabupaten', 'Belitung', '33419'),
(57, 57, 2, 'Bangka Belitung', 'Kabupaten', 'Belitung Timur', '33519'),
(58, 58, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Belu', '85711'),
(59, 59, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Bener Meriah', '24581'),
(60, 60, 26, 'Riau', 'Kabupaten', 'Bengkalis', '28719'),
(61, 61, 12, 'Kalimantan Barat', 'Kabupaten', 'Bengkayang', '79213'),
(62, 62, 4, 'Bengkulu', 'Kota', 'Bengkulu', '38229'),
(63, 63, 4, 'Bengkulu', 'Kabupaten', 'Bengkulu Selatan', '38519'),
(64, 64, 4, 'Bengkulu', 'Kabupaten', 'Bengkulu Tengah', '38319'),
(65, 65, 4, 'Bengkulu', 'Kabupaten', 'Bengkulu Utara', '38619'),
(66, 66, 15, 'Kalimantan Timur', 'Kabupaten', 'Berau', '77311'),
(67, 67, 24, 'Papua', 'Kabupaten', 'Biak Numfor', '98119'),
(68, 68, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Bima', '84171'),
(69, 69, 22, 'Nusa Tenggara Barat (NTB)', 'Kota', 'Bima', '84139'),
(70, 70, 34, 'Sumatera Utara', 'Kota', 'Binjai', '20712'),
(71, 71, 17, 'Kepulauan Riau', 'Kabupaten', 'Bintan', '29135'),
(72, 72, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Bireuen', '24219'),
(73, 73, 31, 'Sulawesi Utara', 'Kota', 'Bitung', '95512'),
(74, 74, 11, 'Jawa Timur', 'Kabupaten', 'Blitar', '66171'),
(75, 75, 11, 'Jawa Timur', 'Kota', 'Blitar', '66124'),
(76, 76, 10, 'Jawa Tengah', 'Kabupaten', 'Blora', '58219'),
(77, 77, 7, 'Gorontalo', 'Kabupaten', 'Boalemo', '96319'),
(78, 78, 9, 'Jawa Barat', 'Kabupaten', 'Bogor', '16911'),
(79, 79, 9, 'Jawa Barat', 'Kota', 'Bogor', '16119'),
(80, 80, 11, 'Jawa Timur', 'Kabupaten', 'Bojonegoro', '62119'),
(81, 81, 31, 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow (Bolmong)', '95755'),
(82, 82, 31, 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow Selatan', '95774'),
(83, 83, 31, 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow Timur', '95783'),
(84, 84, 31, 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow Utara', '95765'),
(85, 85, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Bombana', '93771'),
(86, 86, 11, 'Jawa Timur', 'Kabupaten', 'Bondowoso', '68219'),
(87, 87, 28, 'Sulawesi Selatan', 'Kabupaten', 'Bone', '92713'),
(88, 88, 7, 'Gorontalo', 'Kabupaten', 'Bone Bolango', '96511'),
(89, 89, 15, 'Kalimantan Timur', 'Kota', 'Bontang', '75313'),
(90, 90, 24, 'Papua', 'Kabupaten', 'Boven Digoel', '99662'),
(91, 91, 10, 'Jawa Tengah', 'Kabupaten', 'Boyolali', '57312'),
(92, 92, 10, 'Jawa Tengah', 'Kabupaten', 'Brebes', '52212'),
(93, 93, 32, 'Sumatera Barat', 'Kota', 'Bukittinggi', '26115'),
(94, 94, 1, 'Bali', 'Kabupaten', 'Buleleng', '81111'),
(95, 95, 28, 'Sulawesi Selatan', 'Kabupaten', 'Bulukumba', '92511'),
(96, 96, 16, 'Kalimantan Utara', 'Kabupaten', 'Bulungan (Bulongan)', '77211'),
(97, 97, 8, 'Jambi', 'Kabupaten', 'Bungo', '37216'),
(98, 98, 29, 'Sulawesi Tengah', 'Kabupaten', 'Buol', '94564'),
(99, 99, 19, 'Maluku', 'Kabupaten', 'Buru', '97371'),
(100, 100, 19, 'Maluku', 'Kabupaten', 'Buru Selatan', '97351'),
(101, 101, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Buton', '93754'),
(102, 102, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Buton Utara', '93745'),
(103, 103, 9, 'Jawa Barat', 'Kabupaten', 'Ciamis', '46211'),
(104, 104, 9, 'Jawa Barat', 'Kabupaten', 'Cianjur', '43217'),
(105, 105, 10, 'Jawa Tengah', 'Kabupaten', 'Cilacap', '53211'),
(106, 106, 3, 'Banten', 'Kota', 'Cilegon', '42417'),
(107, 107, 9, 'Jawa Barat', 'Kota', 'Cimahi', '40512'),
(108, 108, 9, 'Jawa Barat', 'Kabupaten', 'Cirebon', '45611'),
(109, 109, 9, 'Jawa Barat', 'Kota', 'Cirebon', '45116'),
(110, 110, 34, 'Sumatera Utara', 'Kabupaten', 'Dairi', '22211'),
(111, 111, 24, 'Papua', 'Kabupaten', 'Deiyai (Deliyai)', '98784'),
(112, 112, 34, 'Sumatera Utara', 'Kabupaten', 'Deli Serdang', '20511'),
(113, 113, 10, 'Jawa Tengah', 'Kabupaten', 'Demak', '59519'),
(114, 114, 1, 'Bali', 'Kota', 'Denpasar', '80227'),
(115, 115, 9, 'Jawa Barat', 'Kota', 'Depok', '16416'),
(116, 116, 32, 'Sumatera Barat', 'Kabupaten', 'Dharmasraya', '27612'),
(117, 117, 24, 'Papua', 'Kabupaten', 'Dogiyai', '98866'),
(118, 118, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Dompu', '84217'),
(119, 119, 29, 'Sulawesi Tengah', 'Kabupaten', 'Donggala', '94341'),
(120, 120, 26, 'Riau', 'Kota', 'Dumai', '28811'),
(121, 121, 33, 'Sumatera Selatan', 'Kabupaten', 'Empat Lawang', '31811'),
(122, 122, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Ende', '86351'),
(123, 123, 28, 'Sulawesi Selatan', 'Kabupaten', 'Enrekang', '91719'),
(124, 124, 25, 'Papua Barat', 'Kabupaten', 'Fakfak', '98651'),
(125, 125, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Flores Timur', '86213'),
(126, 126, 9, 'Jawa Barat', 'Kabupaten', 'Garut', '44126'),
(127, 127, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Gayo Lues', '24653'),
(128, 128, 1, 'Bali', 'Kabupaten', 'Gianyar', '80519'),
(129, 129, 7, 'Gorontalo', 'Kabupaten', 'Gorontalo', '96218'),
(130, 130, 7, 'Gorontalo', 'Kota', 'Gorontalo', '96115'),
(131, 131, 7, 'Gorontalo', 'Kabupaten', 'Gorontalo Utara', '96611'),
(132, 132, 28, 'Sulawesi Selatan', 'Kabupaten', 'Gowa', '92111'),
(133, 133, 11, 'Jawa Timur', 'Kabupaten', 'Gresik', '61115'),
(134, 134, 10, 'Jawa Tengah', 'Kabupaten', 'Grobogan', '58111'),
(135, 135, 5, 'DI Yogyakarta', 'Kabupaten', 'Gunung Kidul', '55812'),
(136, 136, 14, 'Kalimantan Tengah', 'Kabupaten', 'Gunung Mas', '74511'),
(137, 137, 34, 'Sumatera Utara', 'Kota', 'Gunungsitoli', '22813'),
(138, 138, 20, 'Maluku Utara', 'Kabupaten', 'Halmahera Barat', '97757'),
(139, 139, 20, 'Maluku Utara', 'Kabupaten', 'Halmahera Selatan', '97911'),
(140, 140, 20, 'Maluku Utara', 'Kabupaten', 'Halmahera Tengah', '97853'),
(141, 141, 20, 'Maluku Utara', 'Kabupaten', 'Halmahera Timur', '97862'),
(142, 142, 20, 'Maluku Utara', 'Kabupaten', 'Halmahera Utara', '97762'),
(143, 143, 13, 'Kalimantan Selatan', 'Kabupaten', 'Hulu Sungai Selatan', '71212'),
(144, 144, 13, 'Kalimantan Selatan', 'Kabupaten', 'Hulu Sungai Tengah', '71313'),
(145, 145, 13, 'Kalimantan Selatan', 'Kabupaten', 'Hulu Sungai Utara', '71419'),
(146, 146, 34, 'Sumatera Utara', 'Kabupaten', 'Humbang Hasundutan', '22457'),
(147, 147, 26, 'Riau', 'Kabupaten', 'Indragiri Hilir', '29212'),
(148, 148, 26, 'Riau', 'Kabupaten', 'Indragiri Hulu', '29319'),
(149, 149, 9, 'Jawa Barat', 'Kabupaten', 'Indramayu', '45214'),
(150, 150, 24, 'Papua', 'Kabupaten', 'Intan Jaya', '98771'),
(151, 151, 6, 'DKI Jakarta', 'Kota', 'Jakarta Barat', '11220'),
(152, 152, 6, 'DKI Jakarta', 'Kota', 'Jakarta Pusat', '10540'),
(153, 153, 6, 'DKI Jakarta', 'Kota', 'Jakarta Selatan', '12230'),
(154, 154, 6, 'DKI Jakarta', 'Kota', 'Jakarta Timur', '13330'),
(155, 155, 6, 'DKI Jakarta', 'Kota', 'Jakarta Utara', '14140'),
(156, 156, 8, 'Jambi', 'Kota', 'Jambi', '36111'),
(157, 157, 24, 'Papua', 'Kabupaten', 'Jayapura', '99352'),
(158, 158, 24, 'Papua', 'Kota', 'Jayapura', '99114'),
(159, 159, 24, 'Papua', 'Kabupaten', 'Jayawijaya', '99511'),
(160, 160, 11, 'Jawa Timur', 'Kabupaten', 'Jember', '68113'),
(161, 161, 1, 'Bali', 'Kabupaten', 'Jembrana', '82251'),
(162, 162, 28, 'Sulawesi Selatan', 'Kabupaten', 'Jeneponto', '92319'),
(163, 163, 10, 'Jawa Tengah', 'Kabupaten', 'Jepara', '59419'),
(164, 164, 11, 'Jawa Timur', 'Kabupaten', 'Jombang', '61415'),
(165, 165, 25, 'Papua Barat', 'Kabupaten', 'Kaimana', '98671'),
(166, 166, 26, 'Riau', 'Kabupaten', 'Kampar', '28411'),
(167, 167, 14, 'Kalimantan Tengah', 'Kabupaten', 'Kapuas', '73583'),
(168, 168, 12, 'Kalimantan Barat', 'Kabupaten', 'Kapuas Hulu', '78719'),
(169, 169, 10, 'Jawa Tengah', 'Kabupaten', 'Karanganyar', '57718'),
(170, 170, 1, 'Bali', 'Kabupaten', 'Karangasem', '80819'),
(171, 171, 9, 'Jawa Barat', 'Kabupaten', 'Karawang', '41311'),
(172, 172, 17, 'Kepulauan Riau', 'Kabupaten', 'Karimun', '29611'),
(173, 173, 34, 'Sumatera Utara', 'Kabupaten', 'Karo', '22119'),
(174, 174, 14, 'Kalimantan Tengah', 'Kabupaten', 'Katingan', '74411'),
(175, 175, 4, 'Bengkulu', 'Kabupaten', 'Kaur', '38911'),
(176, 176, 12, 'Kalimantan Barat', 'Kabupaten', 'Kayong Utara', '78852'),
(177, 177, 10, 'Jawa Tengah', 'Kabupaten', 'Kebumen', '54319'),
(178, 178, 11, 'Jawa Timur', 'Kabupaten', 'Kediri', '64184'),
(179, 179, 11, 'Jawa Timur', 'Kota', 'Kediri', '64125'),
(180, 180, 24, 'Papua', 'Kabupaten', 'Keerom', '99461'),
(181, 181, 10, 'Jawa Tengah', 'Kabupaten', 'Kendal', '51314'),
(182, 182, 30, 'Sulawesi Tenggara', 'Kota', 'Kendari', '93126'),
(183, 183, 4, 'Bengkulu', 'Kabupaten', 'Kepahiang', '39319'),
(184, 184, 17, 'Kepulauan Riau', 'Kabupaten', 'Kepulauan Anambas', '29991'),
(185, 185, 19, 'Maluku', 'Kabupaten', 'Kepulauan Aru', '97681'),
(186, 186, 32, 'Sumatera Barat', 'Kabupaten', 'Kepulauan Mentawai', '25771'),
(187, 187, 26, 'Riau', 'Kabupaten', 'Kepulauan Meranti', '28791'),
(188, 188, 31, 'Sulawesi Utara', 'Kabupaten', 'Kepulauan Sangihe', '95819'),
(189, 189, 6, 'DKI Jakarta', 'Kabupaten', 'Kepulauan Seribu', '14550'),
(190, 190, 31, 'Sulawesi Utara', 'Kabupaten', 'Kepulauan Siau Tagulandang Biaro (Sitaro)', '95862'),
(191, 191, 20, 'Maluku Utara', 'Kabupaten', 'Kepulauan Sula', '97995'),
(192, 192, 31, 'Sulawesi Utara', 'Kabupaten', 'Kepulauan Talaud', '95885'),
(193, 193, 24, 'Papua', 'Kabupaten', 'Kepulauan Yapen (Yapen Waropen)', '98211'),
(194, 194, 8, 'Jambi', 'Kabupaten', 'Kerinci', '37167'),
(195, 195, 12, 'Kalimantan Barat', 'Kabupaten', 'Ketapang', '78874'),
(196, 196, 10, 'Jawa Tengah', 'Kabupaten', 'Klaten', '57411'),
(197, 197, 1, 'Bali', 'Kabupaten', 'Klungkung', '80719'),
(198, 198, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Kolaka', '93511'),
(199, 199, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Kolaka Utara', '93911'),
(200, 200, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Konawe', '93411'),
(201, 201, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Konawe Selatan', '93811'),
(202, 202, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Konawe Utara', '93311'),
(203, 203, 13, 'Kalimantan Selatan', 'Kabupaten', 'Kotabaru', '72119'),
(204, 204, 31, 'Sulawesi Utara', 'Kota', 'Kotamobagu', '95711'),
(205, 205, 14, 'Kalimantan Tengah', 'Kabupaten', 'Kotawaringin Barat', '74119'),
(206, 206, 14, 'Kalimantan Tengah', 'Kabupaten', 'Kotawaringin Timur', '74364'),
(207, 207, 26, 'Riau', 'Kabupaten', 'Kuantan Singingi', '29519'),
(208, 208, 12, 'Kalimantan Barat', 'Kabupaten', 'Kubu Raya', '78311'),
(209, 209, 10, 'Jawa Tengah', 'Kabupaten', 'Kudus', '59311'),
(210, 210, 5, 'DI Yogyakarta', 'Kabupaten', 'Kulon Progo', '55611'),
(211, 211, 9, 'Jawa Barat', 'Kabupaten', 'Kuningan', '45511'),
(212, 212, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Kupang', '85362'),
(213, 213, 23, 'Nusa Tenggara Timur (NTT)', 'Kota', 'Kupang', '85119'),
(214, 214, 15, 'Kalimantan Timur', 'Kabupaten', 'Kutai Barat', '75711'),
(215, 215, 15, 'Kalimantan Timur', 'Kabupaten', 'Kutai Kartanegara', '75511'),
(216, 216, 15, 'Kalimantan Timur', 'Kabupaten', 'Kutai Timur', '75611'),
(217, 217, 34, 'Sumatera Utara', 'Kabupaten', 'Labuhan Batu', '21412'),
(218, 218, 34, 'Sumatera Utara', 'Kabupaten', 'Labuhan Batu Selatan', '21511'),
(219, 219, 34, 'Sumatera Utara', 'Kabupaten', 'Labuhan Batu Utara', '21711'),
(220, 220, 33, 'Sumatera Selatan', 'Kabupaten', 'Lahat', '31419'),
(221, 221, 14, 'Kalimantan Tengah', 'Kabupaten', 'Lamandau', '74611'),
(222, 222, 11, 'Jawa Timur', 'Kabupaten', 'Lamongan', '64125'),
(223, 223, 18, 'Lampung', 'Kabupaten', 'Lampung Barat', '34814'),
(224, 224, 18, 'Lampung', 'Kabupaten', 'Lampung Selatan', '35511'),
(225, 225, 18, 'Lampung', 'Kabupaten', 'Lampung Tengah', '34212'),
(226, 226, 18, 'Lampung', 'Kabupaten', 'Lampung Timur', '34319'),
(227, 227, 18, 'Lampung', 'Kabupaten', 'Lampung Utara', '34516'),
(228, 228, 12, 'Kalimantan Barat', 'Kabupaten', 'Landak', '78319'),
(229, 229, 34, 'Sumatera Utara', 'Kabupaten', 'Langkat', '20811'),
(230, 230, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Langsa', '24412'),
(231, 231, 24, 'Papua', 'Kabupaten', 'Lanny Jaya', '99531'),
(232, 232, 3, 'Banten', 'Kabupaten', 'Lebak', '42319'),
(233, 233, 4, 'Bengkulu', 'Kabupaten', 'Lebong', '39264'),
(234, 234, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Lembata', '86611'),
(235, 235, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Lhokseumawe', '24352'),
(236, 236, 32, 'Sumatera Barat', 'Kabupaten', 'Lima Puluh Koto/Kota', '26671'),
(237, 237, 17, 'Kepulauan Riau', 'Kabupaten', 'Lingga', '29811'),
(238, 238, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Barat', '83311'),
(239, 239, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Tengah', '83511'),
(240, 240, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Timur', '83612'),
(241, 241, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Utara', '83711'),
(242, 242, 33, 'Sumatera Selatan', 'Kota', 'Lubuk Linggau', '31614'),
(243, 243, 11, 'Jawa Timur', 'Kabupaten', 'Lumajang', '67319'),
(244, 244, 28, 'Sulawesi Selatan', 'Kabupaten', 'Luwu', '91994'),
(245, 245, 28, 'Sulawesi Selatan', 'Kabupaten', 'Luwu Timur', '92981'),
(246, 246, 28, 'Sulawesi Selatan', 'Kabupaten', 'Luwu Utara', '92911'),
(247, 247, 11, 'Jawa Timur', 'Kabupaten', 'Madiun', '63153'),
(248, 248, 11, 'Jawa Timur', 'Kota', 'Madiun', '63122'),
(249, 249, 10, 'Jawa Tengah', 'Kabupaten', 'Magelang', '56519'),
(250, 250, 10, 'Jawa Tengah', 'Kota', 'Magelang', '56133'),
(251, 251, 11, 'Jawa Timur', 'Kabupaten', 'Magetan', '63314'),
(252, 252, 9, 'Jawa Barat', 'Kabupaten', 'Majalengka', '45412'),
(253, 253, 27, 'Sulawesi Barat', 'Kabupaten', 'Majene', '91411'),
(254, 254, 28, 'Sulawesi Selatan', 'Kota', 'Makassar', '90111'),
(255, 255, 11, 'Jawa Timur', 'Kabupaten', 'Malang', '65163'),
(256, 256, 11, 'Jawa Timur', 'Kota', 'Malang', '65112'),
(257, 257, 16, 'Kalimantan Utara', 'Kabupaten', 'Malinau', '77511'),
(258, 258, 19, 'Maluku', 'Kabupaten', 'Maluku Barat Daya', '97451'),
(259, 259, 19, 'Maluku', 'Kabupaten', 'Maluku Tengah', '97513'),
(260, 260, 19, 'Maluku', 'Kabupaten', 'Maluku Tenggara', '97651'),
(261, 261, 19, 'Maluku', 'Kabupaten', 'Maluku Tenggara Barat', '97465'),
(262, 262, 27, 'Sulawesi Barat', 'Kabupaten', 'Mamasa', '91362'),
(263, 263, 24, 'Papua', 'Kabupaten', 'Mamberamo Raya', '99381'),
(264, 264, 24, 'Papua', 'Kabupaten', 'Mamberamo Tengah', '99553'),
(265, 265, 27, 'Sulawesi Barat', 'Kabupaten', 'Mamuju', '91519'),
(266, 266, 27, 'Sulawesi Barat', 'Kabupaten', 'Mamuju Utara', '91571'),
(267, 267, 31, 'Sulawesi Utara', 'Kota', 'Manado', '95247'),
(268, 268, 34, 'Sumatera Utara', 'Kabupaten', 'Mandailing Natal', '22916'),
(269, 269, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Manggarai', '86551'),
(270, 270, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Manggarai Barat', '86711'),
(271, 271, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Manggarai Timur', '86811'),
(272, 272, 25, 'Papua Barat', 'Kabupaten', 'Manokwari', '98311'),
(273, 273, 25, 'Papua Barat', 'Kabupaten', 'Manokwari Selatan', '98355'),
(274, 274, 24, 'Papua', 'Kabupaten', 'Mappi', '99853'),
(275, 275, 28, 'Sulawesi Selatan', 'Kabupaten', 'Maros', '90511'),
(276, 276, 22, 'Nusa Tenggara Barat (NTB)', 'Kota', 'Mataram', '83131'),
(277, 277, 25, 'Papua Barat', 'Kabupaten', 'Maybrat', '98051'),
(278, 278, 34, 'Sumatera Utara', 'Kota', 'Medan', '20228'),
(279, 279, 12, 'Kalimantan Barat', 'Kabupaten', 'Melawi', '78619'),
(280, 280, 8, 'Jambi', 'Kabupaten', 'Merangin', '37319'),
(281, 281, 24, 'Papua', 'Kabupaten', 'Merauke', '99613'),
(282, 282, 18, 'Lampung', 'Kabupaten', 'Mesuji', '34911'),
(283, 283, 18, 'Lampung', 'Kota', 'Metro', '34111'),
(284, 284, 24, 'Papua', 'Kabupaten', 'Mimika', '99962'),
(285, 285, 31, 'Sulawesi Utara', 'Kabupaten', 'Minahasa', '95614'),
(286, 286, 31, 'Sulawesi Utara', 'Kabupaten', 'Minahasa Selatan', '95914'),
(287, 287, 31, 'Sulawesi Utara', 'Kabupaten', 'Minahasa Tenggara', '95995'),
(288, 288, 31, 'Sulawesi Utara', 'Kabupaten', 'Minahasa Utara', '95316'),
(289, 289, 11, 'Jawa Timur', 'Kabupaten', 'Mojokerto', '61382'),
(290, 290, 11, 'Jawa Timur', 'Kota', 'Mojokerto', '61316'),
(291, 291, 29, 'Sulawesi Tengah', 'Kabupaten', 'Morowali', '94911'),
(292, 292, 33, 'Sumatera Selatan', 'Kabupaten', 'Muara Enim', '31315'),
(293, 293, 8, 'Jambi', 'Kabupaten', 'Muaro Jambi', '36311'),
(294, 294, 4, 'Bengkulu', 'Kabupaten', 'Muko Muko', '38715'),
(295, 295, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Muna', '93611'),
(296, 296, 14, 'Kalimantan Tengah', 'Kabupaten', 'Murung Raya', '73911'),
(297, 297, 33, 'Sumatera Selatan', 'Kabupaten', 'Musi Banyuasin', '30719'),
(298, 298, 33, 'Sumatera Selatan', 'Kabupaten', 'Musi Rawas', '31661'),
(299, 299, 24, 'Papua', 'Kabupaten', 'Nabire', '98816'),
(300, 300, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Nagan Raya', '23674'),
(301, 301, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Nagekeo', '86911'),
(302, 302, 17, 'Kepulauan Riau', 'Kabupaten', 'Natuna', '29711'),
(303, 303, 24, 'Papua', 'Kabupaten', 'Nduga', '99541'),
(304, 304, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Ngada', '86413'),
(305, 305, 11, 'Jawa Timur', 'Kabupaten', 'Nganjuk', '64414'),
(306, 306, 11, 'Jawa Timur', 'Kabupaten', 'Ngawi', '63219'),
(307, 307, 34, 'Sumatera Utara', 'Kabupaten', 'Nias', '22876'),
(308, 308, 34, 'Sumatera Utara', 'Kabupaten', 'Nias Barat', '22895'),
(309, 309, 34, 'Sumatera Utara', 'Kabupaten', 'Nias Selatan', '22865'),
(310, 310, 34, 'Sumatera Utara', 'Kabupaten', 'Nias Utara', '22856'),
(311, 311, 16, 'Kalimantan Utara', 'Kabupaten', 'Nunukan', '77421'),
(312, 312, 33, 'Sumatera Selatan', 'Kabupaten', 'Ogan Ilir', '30811'),
(313, 313, 33, 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ilir', '30618'),
(314, 314, 33, 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ulu', '32112'),
(315, 315, 33, 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ulu Selatan', '32211'),
(316, 316, 33, 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ulu Timur', '32312'),
(317, 317, 11, 'Jawa Timur', 'Kabupaten', 'Pacitan', '63512'),
(318, 318, 32, 'Sumatera Barat', 'Kota', 'Padang', '25112'),
(319, 319, 34, 'Sumatera Utara', 'Kabupaten', 'Padang Lawas', '22763'),
(320, 320, 34, 'Sumatera Utara', 'Kabupaten', 'Padang Lawas Utara', '22753'),
(321, 321, 32, 'Sumatera Barat', 'Kota', 'Padang Panjang', '27122'),
(322, 322, 32, 'Sumatera Barat', 'Kabupaten', 'Padang Pariaman', '25583'),
(323, 323, 34, 'Sumatera Utara', 'Kota', 'Padang Sidempuan', '22727'),
(324, 324, 33, 'Sumatera Selatan', 'Kota', 'Pagar Alam', '31512'),
(325, 325, 34, 'Sumatera Utara', 'Kabupaten', 'Pakpak Bharat', '22272'),
(326, 326, 14, 'Kalimantan Tengah', 'Kota', 'Palangka Raya', '73112'),
(327, 327, 33, 'Sumatera Selatan', 'Kota', 'Palembang', '30111'),
(328, 328, 28, 'Sulawesi Selatan', 'Kota', 'Palopo', '91911'),
(329, 329, 29, 'Sulawesi Tengah', 'Kota', 'Palu', '94111'),
(330, 330, 11, 'Jawa Timur', 'Kabupaten', 'Pamekasan', '69319'),
(331, 331, 3, 'Banten', 'Kabupaten', 'Pandeglang', '42212'),
(332, 332, 9, 'Jawa Barat', 'Kabupaten', 'Pangandaran', '46511'),
(333, 333, 28, 'Sulawesi Selatan', 'Kabupaten', 'Pangkajene Kepulauan', '90611'),
(334, 334, 2, 'Bangka Belitung', 'Kota', 'Pangkal Pinang', '33115'),
(335, 335, 24, 'Papua', 'Kabupaten', 'Paniai', '98765'),
(336, 336, 28, 'Sulawesi Selatan', 'Kota', 'Parepare', '91123'),
(337, 337, 32, 'Sumatera Barat', 'Kota', 'Pariaman', '25511'),
(338, 338, 29, 'Sulawesi Tengah', 'Kabupaten', 'Parigi Moutong', '94411'),
(339, 339, 32, 'Sumatera Barat', 'Kabupaten', 'Pasaman', '26318'),
(340, 340, 32, 'Sumatera Barat', 'Kabupaten', 'Pasaman Barat', '26511'),
(341, 341, 15, 'Kalimantan Timur', 'Kabupaten', 'Paser', '76211'),
(342, 342, 11, 'Jawa Timur', 'Kabupaten', 'Pasuruan', '67153'),
(343, 343, 11, 'Jawa Timur', 'Kota', 'Pasuruan', '67118'),
(344, 344, 10, 'Jawa Tengah', 'Kabupaten', 'Pati', '59114'),
(345, 345, 32, 'Sumatera Barat', 'Kota', 'Payakumbuh', '26213'),
(346, 346, 25, 'Papua Barat', 'Kabupaten', 'Pegunungan Arfak', '98354'),
(347, 347, 24, 'Papua', 'Kabupaten', 'Pegunungan Bintang', '99573'),
(348, 348, 10, 'Jawa Tengah', 'Kabupaten', 'Pekalongan', '51161'),
(349, 349, 10, 'Jawa Tengah', 'Kota', 'Pekalongan', '51122'),
(350, 350, 26, 'Riau', 'Kota', 'Pekanbaru', '28112'),
(351, 351, 26, 'Riau', 'Kabupaten', 'Pelalawan', '28311'),
(352, 352, 10, 'Jawa Tengah', 'Kabupaten', 'Pemalang', '52319'),
(353, 353, 34, 'Sumatera Utara', 'Kota', 'Pematang Siantar', '21126'),
(354, 354, 15, 'Kalimantan Timur', 'Kabupaten', 'Penajam Paser Utara', '76311'),
(355, 355, 18, 'Lampung', 'Kabupaten', 'Pesawaran', '35312'),
(356, 356, 18, 'Lampung', 'Kabupaten', 'Pesisir Barat', '35974'),
(357, 357, 32, 'Sumatera Barat', 'Kabupaten', 'Pesisir Selatan', '25611'),
(358, 358, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Pidie', '24116'),
(359, 359, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Pidie Jaya', '24186'),
(360, 360, 28, 'Sulawesi Selatan', 'Kabupaten', 'Pinrang', '91251'),
(361, 361, 7, 'Gorontalo', 'Kabupaten', 'Pohuwato', '96419'),
(362, 362, 27, 'Sulawesi Barat', 'Kabupaten', 'Polewali Mandar', '91311'),
(363, 363, 11, 'Jawa Timur', 'Kabupaten', 'Ponorogo', '63411'),
(364, 364, 12, 'Kalimantan Barat', 'Kabupaten', 'Pontianak', '78971'),
(365, 365, 12, 'Kalimantan Barat', 'Kota', 'Pontianak', '78112'),
(366, 366, 29, 'Sulawesi Tengah', 'Kabupaten', 'Poso', '94615'),
(367, 367, 33, 'Sumatera Selatan', 'Kota', 'Prabumulih', '31121'),
(368, 368, 18, 'Lampung', 'Kabupaten', 'Pringsewu', '35719'),
(369, 369, 11, 'Jawa Timur', 'Kabupaten', 'Probolinggo', '67282'),
(370, 370, 11, 'Jawa Timur', 'Kota', 'Probolinggo', '67215'),
(371, 371, 14, 'Kalimantan Tengah', 'Kabupaten', 'Pulang Pisau', '74811'),
(372, 372, 20, 'Maluku Utara', 'Kabupaten', 'Pulau Morotai', '97771'),
(373, 373, 24, 'Papua', 'Kabupaten', 'Puncak', '98981'),
(374, 374, 24, 'Papua', 'Kabupaten', 'Puncak Jaya', '98979'),
(375, 375, 10, 'Jawa Tengah', 'Kabupaten', 'Purbalingga', '53312'),
(376, 376, 9, 'Jawa Barat', 'Kabupaten', 'Purwakarta', '41119'),
(377, 377, 10, 'Jawa Tengah', 'Kabupaten', 'Purworejo', '54111'),
(378, 378, 25, 'Papua Barat', 'Kabupaten', 'Raja Ampat', '98489'),
(379, 379, 4, 'Bengkulu', 'Kabupaten', 'Rejang Lebong', '39112'),
(380, 380, 10, 'Jawa Tengah', 'Kabupaten', 'Rembang', '59219'),
(381, 381, 26, 'Riau', 'Kabupaten', 'Rokan Hilir', '28992'),
(382, 382, 26, 'Riau', 'Kabupaten', 'Rokan Hulu', '28511'),
(383, 383, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Rote Ndao', '85982'),
(384, 384, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Sabang', '23512'),
(385, 385, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sabu Raijua', '85391'),
(386, 386, 10, 'Jawa Tengah', 'Kota', 'Salatiga', '50711'),
(387, 387, 15, 'Kalimantan Timur', 'Kota', 'Samarinda', '75133'),
(388, 388, 12, 'Kalimantan Barat', 'Kabupaten', 'Sambas', '79453'),
(389, 389, 34, 'Sumatera Utara', 'Kabupaten', 'Samosir', '22392'),
(390, 390, 11, 'Jawa Timur', 'Kabupaten', 'Sampang', '69219'),
(391, 391, 12, 'Kalimantan Barat', 'Kabupaten', 'Sanggau', '78557'),
(392, 392, 24, 'Papua', 'Kabupaten', 'Sarmi', '99373'),
(393, 393, 8, 'Jambi', 'Kabupaten', 'Sarolangun', '37419'),
(394, 394, 32, 'Sumatera Barat', 'Kota', 'Sawah Lunto', '27416'),
(395, 395, 12, 'Kalimantan Barat', 'Kabupaten', 'Sekadau', '79583'),
(396, 396, 28, 'Sulawesi Selatan', 'Kabupaten', 'Selayar (Kepulauan Selayar)', '92812'),
(397, 397, 4, 'Bengkulu', 'Kabupaten', 'Seluma', '38811'),
(398, 398, 10, 'Jawa Tengah', 'Kabupaten', 'Semarang', '50511'),
(399, 399, 10, 'Jawa Tengah', 'Kota', 'Semarang', '50135'),
(400, 400, 19, 'Maluku', 'Kabupaten', 'Seram Bagian Barat', '97561'),
(401, 401, 19, 'Maluku', 'Kabupaten', 'Seram Bagian Timur', '97581'),
(402, 402, 3, 'Banten', 'Kabupaten', 'Serang', '42182'),
(403, 403, 3, 'Banten', 'Kota', 'Serang', '42111'),
(404, 404, 34, 'Sumatera Utara', 'Kabupaten', 'Serdang Bedagai', '20915'),
(405, 405, 14, 'Kalimantan Tengah', 'Kabupaten', 'Seruyan', '74211'),
(406, 406, 26, 'Riau', 'Kabupaten', 'Siak', '28623'),
(407, 407, 34, 'Sumatera Utara', 'Kota', 'Sibolga', '22522'),
(408, 408, 28, 'Sulawesi Selatan', 'Kabupaten', 'Sidenreng Rappang/Rapang', '91613'),
(409, 409, 11, 'Jawa Timur', 'Kabupaten', 'Sidoarjo', '61219'),
(410, 410, 29, 'Sulawesi Tengah', 'Kabupaten', 'Sigi', '94364'),
(411, 411, 32, 'Sumatera Barat', 'Kabupaten', 'Sijunjung (Sawah Lunto Sijunjung)', '27511'),
(412, 412, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sikka', '86121'),
(413, 413, 34, 'Sumatera Utara', 'Kabupaten', 'Simalungun', '21162'),
(414, 414, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Simeulue', '23891'),
(415, 415, 12, 'Kalimantan Barat', 'Kota', 'Singkawang', '79117'),
(416, 416, 28, 'Sulawesi Selatan', 'Kabupaten', 'Sinjai', '92615'),
(417, 417, 12, 'Kalimantan Barat', 'Kabupaten', 'Sintang', '78619'),
(418, 418, 11, 'Jawa Timur', 'Kabupaten', 'Situbondo', '68316'),
(419, 419, 5, 'DI Yogyakarta', 'Kabupaten', 'Sleman', '55513'),
(420, 420, 32, 'Sumatera Barat', 'Kabupaten', 'Solok', '27365'),
(421, 421, 32, 'Sumatera Barat', 'Kota', 'Solok', '27315'),
(422, 422, 32, 'Sumatera Barat', 'Kabupaten', 'Solok Selatan', '27779'),
(423, 423, 28, 'Sulawesi Selatan', 'Kabupaten', 'Soppeng', '90812'),
(424, 424, 25, 'Papua Barat', 'Kabupaten', 'Sorong', '98431'),
(425, 425, 25, 'Papua Barat', 'Kota', 'Sorong', '98411'),
(426, 426, 25, 'Papua Barat', 'Kabupaten', 'Sorong Selatan', '98454'),
(427, 427, 10, 'Jawa Tengah', 'Kabupaten', 'Sragen', '57211'),
(428, 428, 9, 'Jawa Barat', 'Kabupaten', 'Subang', '41215'),
(429, 429, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Subulussalam', '24882'),
(430, 430, 9, 'Jawa Barat', 'Kabupaten', 'Sukabumi', '43311'),
(431, 431, 9, 'Jawa Barat', 'Kota', 'Sukabumi', '43114'),
(432, 432, 14, 'Kalimantan Tengah', 'Kabupaten', 'Sukamara', '74712'),
(433, 433, 10, 'Jawa Tengah', 'Kabupaten', 'Sukoharjo', '57514'),
(434, 434, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Barat', '87219'),
(435, 435, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Barat Daya', '87453'),
(436, 436, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Tengah', '87358'),
(437, 437, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Timur', '87112'),
(438, 438, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Sumbawa', '84315'),
(439, 439, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Sumbawa Barat', '84419'),
(440, 440, 9, 'Jawa Barat', 'Kabupaten', 'Sumedang', '45326'),
(441, 441, 11, 'Jawa Timur', 'Kabupaten', 'Sumenep', '69413'),
(442, 442, 8, 'Jambi', 'Kota', 'Sungaipenuh', '37113'),
(443, 443, 24, 'Papua', 'Kabupaten', 'Supiori', '98164'),
(444, 444, 11, 'Jawa Timur', 'Kota', 'Surabaya', '60119'),
(445, 445, 10, 'Jawa Tengah', 'Kota', 'Surakarta (Solo)', '57113'),
(446, 446, 13, 'Kalimantan Selatan', 'Kabupaten', 'Tabalong', '71513'),
(447, 447, 1, 'Bali', 'Kabupaten', 'Tabanan', '82119'),
(448, 448, 28, 'Sulawesi Selatan', 'Kabupaten', 'Takalar', '92212'),
(449, 449, 25, 'Papua Barat', 'Kabupaten', 'Tambrauw', '98475'),
(450, 450, 16, 'Kalimantan Utara', 'Kabupaten', 'Tana Tidung', '77611'),
(451, 451, 28, 'Sulawesi Selatan', 'Kabupaten', 'Tana Toraja', '91819'),
(452, 452, 13, 'Kalimantan Selatan', 'Kabupaten', 'Tanah Bumbu', '72211'),
(453, 453, 32, 'Sumatera Barat', 'Kabupaten', 'Tanah Datar', '27211'),
(454, 454, 13, 'Kalimantan Selatan', 'Kabupaten', 'Tanah Laut', '70811'),
(455, 455, 3, 'Banten', 'Kabupaten', 'Tangerang', '15914'),
(456, 456, 3, 'Banten', 'Kota', 'Tangerang', '15111'),
(457, 457, 3, 'Banten', 'Kota', 'Tangerang Selatan', '15332'),
(458, 458, 18, 'Lampung', 'Kabupaten', 'Tanggamus', '35619'),
(459, 459, 34, 'Sumatera Utara', 'Kota', 'Tanjung Balai', '21321'),
(460, 460, 8, 'Jambi', 'Kabupaten', 'Tanjung Jabung Barat', '36513'),
(461, 461, 8, 'Jambi', 'Kabupaten', 'Tanjung Jabung Timur', '36719'),
(462, 462, 17, 'Kepulauan Riau', 'Kota', 'Tanjung Pinang', '29111'),
(463, 463, 34, 'Sumatera Utara', 'Kabupaten', 'Tapanuli Selatan', '22742'),
(464, 464, 34, 'Sumatera Utara', 'Kabupaten', 'Tapanuli Tengah', '22611'),
(465, 465, 34, 'Sumatera Utara', 'Kabupaten', 'Tapanuli Utara', '22414'),
(466, 466, 13, 'Kalimantan Selatan', 'Kabupaten', 'Tapin', '71119'),
(467, 467, 16, 'Kalimantan Utara', 'Kota', 'Tarakan', '77114'),
(468, 468, 9, 'Jawa Barat', 'Kabupaten', 'Tasikmalaya', '46411'),
(469, 469, 9, 'Jawa Barat', 'Kota', 'Tasikmalaya', '46116'),
(470, 470, 34, 'Sumatera Utara', 'Kota', 'Tebing Tinggi', '20632'),
(471, 471, 8, 'Jambi', 'Kabupaten', 'Tebo', '37519'),
(472, 472, 10, 'Jawa Tengah', 'Kabupaten', 'Tegal', '52419'),
(473, 473, 10, 'Jawa Tengah', 'Kota', 'Tegal', '52114'),
(474, 474, 25, 'Papua Barat', 'Kabupaten', 'Teluk Bintuni', '98551'),
(475, 475, 25, 'Papua Barat', 'Kabupaten', 'Teluk Wondama', '98591'),
(476, 476, 10, 'Jawa Tengah', 'Kabupaten', 'Temanggung', '56212'),
(477, 477, 20, 'Maluku Utara', 'Kota', 'Ternate', '97714'),
(478, 478, 20, 'Maluku Utara', 'Kota', 'Tidore Kepulauan', '97815'),
(479, 479, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Timor Tengah Selatan', '85562'),
(480, 480, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Timor Tengah Utara', '85612'),
(481, 481, 34, 'Sumatera Utara', 'Kabupaten', 'Toba Samosir', '22316'),
(482, 482, 29, 'Sulawesi Tengah', 'Kabupaten', 'Tojo Una-Una', '94683'),
(483, 483, 29, 'Sulawesi Tengah', 'Kabupaten', 'Toli-Toli', '94542'),
(484, 484, 24, 'Papua', 'Kabupaten', 'Tolikara', '99411'),
(485, 485, 31, 'Sulawesi Utara', 'Kota', 'Tomohon', '95416'),
(486, 486, 28, 'Sulawesi Selatan', 'Kabupaten', 'Toraja Utara', '91831'),
(487, 487, 11, 'Jawa Timur', 'Kabupaten', 'Trenggalek', '66312'),
(488, 488, 19, 'Maluku', 'Kota', 'Tual', '97612'),
(489, 489, 11, 'Jawa Timur', 'Kabupaten', 'Tuban', '62319'),
(490, 490, 18, 'Lampung', 'Kabupaten', 'Tulang Bawang', '34613'),
(491, 491, 18, 'Lampung', 'Kabupaten', 'Tulang Bawang Barat', '34419'),
(492, 492, 11, 'Jawa Timur', 'Kabupaten', 'Tulungagung', '66212'),
(493, 493, 28, 'Sulawesi Selatan', 'Kabupaten', 'Wajo', '90911'),
(494, 494, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Wakatobi', '93791'),
(495, 495, 24, 'Papua', 'Kabupaten', 'Waropen', '98269'),
(496, 496, 18, 'Lampung', 'Kabupaten', 'Way Kanan', '34711'),
(497, 497, 10, 'Jawa Tengah', 'Kabupaten', 'Wonogiri', '57619'),
(498, 498, 10, 'Jawa Tengah', 'Kabupaten', 'Wonosobo', '56311'),
(499, 499, 24, 'Papua', 'Kabupaten', 'Yahukimo', '99041'),
(500, 500, 24, 'Papua', 'Kabupaten', 'Yalimo', '99481'),
(501, 501, 5, 'DI Yogyakarta', 'Kota', 'Yogyakarta', '55111');

-- --------------------------------------------------------

--
-- Struktur dari tabel `countries`
--

CREATE TABLE `countries` (
  `id` int(10) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'US', 'United States'),
(2, 'CA', 'Canada'),
(3, 'AF', 'Afghanistan'),
(4, 'AL', 'Albania'),
(5, 'DZ', 'Algeria'),
(6, 'AS', 'American Samoa'),
(7, 'AD', 'Andorra'),
(8, 'AO', 'Angola'),
(9, 'AI', 'Anguilla'),
(10, 'AQ', 'Antarctica'),
(11, 'AG', 'Antigua and/or Barbuda'),
(12, 'AR', 'Argentina'),
(13, 'AM', 'Armenia'),
(14, 'AW', 'Aruba'),
(15, 'AU', 'Australia'),
(16, 'AT', 'Austria'),
(17, 'AZ', 'Azerbaijan'),
(18, 'BS', 'Bahamas'),
(19, 'BH', 'Bahrain'),
(20, 'BD', 'Bangladesh'),
(21, 'BB', 'Barbados'),
(22, 'BY', 'Belarus'),
(23, 'BE', 'Belgium'),
(24, 'BZ', 'Belize'),
(25, 'BJ', 'Benin'),
(26, 'BM', 'Bermuda'),
(27, 'BT', 'Bhutan'),
(28, 'BO', 'Bolivia'),
(29, 'BA', 'Bosnia and Herzegovina'),
(30, 'BW', 'Botswana'),
(31, 'BV', 'Bouvet Island'),
(32, 'BR', 'Brazil'),
(33, 'IO', 'British lndian Ocean Territory'),
(34, 'BN', 'Brunei Darussalam'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'KH', 'Cambodia'),
(39, 'CM', 'Cameroon'),
(40, 'CV', 'Cape Verde'),
(41, 'KY', 'Cayman Islands'),
(42, 'CF', 'Central African Republic'),
(43, 'TD', 'Chad'),
(44, 'CL', 'Chile'),
(45, 'CN', 'China'),
(46, 'CX', 'Christmas Island'),
(47, 'CC', 'Cocos (Keeling) Islands'),
(48, 'CO', 'Colombia'),
(49, 'KM', 'Comoros'),
(50, 'CG', 'Congo'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'HR', 'Croatia (Hrvatska)'),
(54, 'CU', 'Cuba'),
(55, 'CY', 'Cyprus'),
(56, 'CZ', 'Czech Republic'),
(57, 'CD', 'Democratic Republic of Congo'),
(58, 'DK', 'Denmark'),
(59, 'DJ', 'Djibouti'),
(60, 'DM', 'Dominica'),
(61, 'DO', 'Dominican Republic'),
(62, 'TP', 'East Timor'),
(63, 'EC', 'Ecudaor'),
(64, 'EG', 'Egypt'),
(65, 'SV', 'El Salvador'),
(66, 'GQ', 'Equatorial Guinea'),
(67, 'ER', 'Eritrea'),
(68, 'EE', 'Estonia'),
(69, 'ET', 'Ethiopia'),
(70, 'FK', 'Falkland Islands (Malvinas)'),
(71, 'FO', 'Faroe Islands'),
(72, 'FJ', 'Fiji'),
(73, 'FI', 'Finland'),
(74, 'FR', 'France'),
(75, 'FX', 'France, Metropolitan'),
(76, 'GF', 'French Guiana'),
(77, 'PF', 'French Polynesia'),
(78, 'TF', 'French Southern Territories'),
(79, 'GA', 'Gabon'),
(80, 'GM', 'Gambia'),
(81, 'GE', 'Georgia'),
(82, 'DE', 'Germany'),
(83, 'GH', 'Ghana'),
(84, 'GI', 'Gibraltar'),
(85, 'GR', 'Greece'),
(86, 'GL', 'Greenland'),
(87, 'GD', 'Grenada'),
(88, 'GP', 'Guadeloupe'),
(89, 'GU', 'Guam'),
(90, 'GT', 'Guatemala'),
(91, 'GN', 'Guinea'),
(92, 'GW', 'Guinea-Bissau'),
(93, 'GY', 'Guyana'),
(94, 'HT', 'Haiti'),
(95, 'HM', 'Heard and Mc Donald Islands'),
(96, 'HN', 'Honduras'),
(97, 'HK', 'Hong Kong'),
(98, 'HU', 'Hungary'),
(99, 'IS', 'Iceland'),
(100, 'IN', 'India'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JM', 'Jamaica'),
(109, 'JP', 'Japan'),
(110, 'JO', 'Jordan'),
(111, 'KZ', 'Kazakhstan'),
(112, 'KE', 'Kenya'),
(113, 'KI', 'Kiribati'),
(114, 'KP', 'Korea, Democratic People\'s Republic of'),
(115, 'KR', 'Korea, Republic of'),
(116, 'KW', 'Kuwait'),
(117, 'KG', 'Kyrgyzstan'),
(118, 'LA', 'Lao People\'s Democratic Republic'),
(119, 'LV', 'Latvia'),
(120, 'LB', 'Lebanon'),
(121, 'LS', 'Lesotho'),
(122, 'LR', 'Liberia'),
(123, 'LY', 'Libyan Arab Jamahiriya'),
(124, 'LI', 'Liechtenstein'),
(125, 'LT', 'Lithuania'),
(126, 'LU', 'Luxembourg'),
(127, 'MO', 'Macau'),
(128, 'MK', 'Macedonia'),
(129, 'MG', 'Madagascar'),
(130, 'MW', 'Malawi'),
(131, 'MY', 'Malaysia'),
(132, 'MV', 'Maldives'),
(133, 'ML', 'Mali'),
(134, 'MT', 'Malta'),
(135, 'MH', 'Marshall Islands'),
(136, 'MQ', 'Martinique'),
(137, 'MR', 'Mauritania'),
(138, 'MU', 'Mauritius'),
(139, 'TY', 'Mayotte'),
(140, 'MX', 'Mexico'),
(141, 'FM', 'Micronesia, Federated States of'),
(142, 'MD', 'Moldova, Republic of'),
(143, 'MC', 'Monaco'),
(144, 'MN', 'Mongolia'),
(145, 'MS', 'Montserrat'),
(146, 'MA', 'Morocco'),
(147, 'MZ', 'Mozambique'),
(148, 'MM', 'Myanmar'),
(149, 'NA', 'Namibia'),
(150, 'NR', 'Nauru'),
(151, 'NP', 'Nepal'),
(152, 'NL', 'Netherlands'),
(153, 'AN', 'Netherlands Antilles'),
(154, 'NC', 'New Caledonia'),
(155, 'NZ', 'New Zealand'),
(156, 'NI', 'Nicaragua'),
(157, 'NE', 'Niger'),
(158, 'NG', 'Nigeria'),
(159, 'NU', 'Niue'),
(160, 'NF', 'Norfork Island'),
(161, 'MP', 'Northern Mariana Islands'),
(162, 'NO', 'Norway'),
(163, 'OM', 'Oman'),
(164, 'PK', 'Pakistan'),
(165, 'PW', 'Palau'),
(166, 'PA', 'Panama'),
(167, 'PG', 'Papua New Guinea'),
(168, 'PY', 'Paraguay'),
(169, 'PE', 'Peru'),
(170, 'PH', 'Philippines'),
(171, 'PN', 'Pitcairn'),
(172, 'PL', 'Poland'),
(173, 'PT', 'Portugal'),
(174, 'PR', 'Puerto Rico'),
(175, 'QA', 'Qatar'),
(176, 'SS', 'Republic of South Sudan'),
(177, 'RE', 'Reunion'),
(178, 'RO', 'Romania'),
(179, 'RU', 'Russian Federation'),
(180, 'RW', 'Rwanda'),
(181, 'KN', 'Saint Kitts and Nevis'),
(182, 'LC', 'Saint Lucia'),
(183, 'VC', 'Saint Vincent and the Grenadines'),
(184, 'WS', 'Samoa'),
(185, 'SM', 'San Marino'),
(186, 'ST', 'Sao Tome and Principe'),
(187, 'SA', 'Saudi Arabia'),
(188, 'SN', 'Senegal'),
(189, 'RS', 'Serbia'),
(190, 'SC', 'Seychelles'),
(191, 'SL', 'Sierra Leone'),
(192, 'SG', 'Singapore'),
(193, 'SK', 'Slovakia'),
(194, 'SI', 'Slovenia'),
(195, 'SB', 'Solomon Islands'),
(196, 'SO', 'Somalia'),
(197, 'ZA', 'South Africa'),
(198, 'GS', 'South Georgia South Sandwich Islands'),
(199, 'ES', 'Spain'),
(200, 'LK', 'Sri Lanka'),
(201, 'SH', 'St. Helena'),
(202, 'PM', 'St. Pierre and Miquelon'),
(203, 'SD', 'Sudan'),
(204, 'SR', 'Suriname'),
(205, 'SJ', 'Svalbarn and Jan Mayen Islands'),
(206, 'SZ', 'Swaziland'),
(207, 'SE', 'Sweden'),
(208, 'CH', 'Switzerland'),
(209, 'SY', 'Syrian Arab Republic'),
(210, 'TW', 'Taiwan'),
(211, 'TJ', 'Tajikistan'),
(212, 'TZ', 'Tanzania, United Republic of'),
(213, 'TH', 'Thailand'),
(214, 'TG', 'Togo'),
(215, 'TK', 'Tokelau'),
(216, 'TO', 'Tonga'),
(217, 'TT', 'Trinidad and Tobago'),
(218, 'TN', 'Tunisia'),
(219, 'TR', 'Turkey'),
(220, 'TM', 'Turkmenistan'),
(221, 'TC', 'Turks and Caicos Islands'),
(222, 'TV', 'Tuvalu'),
(223, 'UG', 'Uganda'),
(224, 'UA', 'Ukraine'),
(225, 'AE', 'United Arab Emirates'),
(226, 'GB', 'United Kingdom'),
(227, 'UM', 'United States minor outlying islands'),
(228, 'UY', 'Uruguay'),
(229, 'UZ', 'Uzbekistan'),
(230, 'VU', 'Vanuatu'),
(231, 'VA', 'Vatican City State'),
(232, 'VE', 'Venezuela'),
(233, 'VN', 'Vietnam'),
(234, 'VG', 'Virgin Islands (British)'),
(235, 'VI', 'Virgin Islands (U.S.)'),
(236, 'WF', 'Wallis and Futuna Islands'),
(237, 'EH', 'Western Sahara'),
(238, 'YE', 'Yemen'),
(239, 'YU', 'Yugoslavia'),
(240, 'ZR', 'Zaire'),
(241, 'ZM', 'Zambia'),
(242, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Struktur dari tabel `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `special_condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_published` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `items`
--

INSERT INTO `items` (`id`, `id_user`, `name`, `quantity`, `price`, `special_condition`, `is_published`, `created_at`, `updated_at`) VALUES
(75, 3, 'sadsad', 217, 233232, 'dfsfwe', '1', '2019-08-31 07:05:31', '2019-09-02 22:46:14'),
(76, 3, 'Bookk3', 16, 20000, 'asdadasdsdad', '1', '2019-08-31 07:28:11', '2019-09-02 21:47:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `item_categories`
--

CREATE TABLE `item_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_item` int(11) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `item_categories`
--

INSERT INTO `item_categories` (`id`, `id_item`, `id_category`, `created_at`, `updated_at`) VALUES
(34, 75, 5, '2019-08-31 07:05:31', '2019-08-31 07:05:31'),
(68, 76, 2, '2019-09-02 21:47:56', '2019-09-02 21:47:56'),
(69, 76, 3, '2019-09-02 21:47:56', '2019-09-02 21:47:56'),
(70, 76, 4, '2019-09-02 21:47:56', '2019-09-02 21:47:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `item_locations`
--

CREATE TABLE `item_locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_item` int(11) UNSIGNED NOT NULL,
  `country_id` int(10) NOT NULL,
  `province_id` int(10) NOT NULL,
  `city_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `item_locations`
--

INSERT INTO `item_locations` (`id`, `id_item`, `country_id`, `province_id`, `city_id`, `created_at`, `updated_at`) VALUES
(31, 75, 2, 2, 29, '2019-08-31 07:05:31', '2019-08-31 07:05:31'),
(32, 76, 101, 4, 63, '2019-09-02 21:47:56', '2019-09-02 21:47:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `item_photos`
--

CREATE TABLE `item_photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_item` int(10) UNSIGNED NOT NULL,
  `pict` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `item_photos`
--

INSERT INTO `item_photos` (`id`, `id_item`, `pict`, `created_at`, `updated_at`) VALUES
(17, 76, 'ITM_aMWGH4_2019-08-30-22:28:04-screenshot.png', '2019-08-31 07:28:11', '2019-08-31 07:28:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_07_25_122035_create_admins_table', 2),
(5, '2019_07_30_054517_create_items_table', 3),
(6, '2019_07_30_055545_create_item_photos_table', 4),
(7, '2019_08_01_125755_create_settings_table', 5),
(8, '2019_08_21_120930_create_cart_storage_table', 6),
(9, '2019_08_22_042958_create_transactions_table', 7),
(10, '2019_08_22_043206_create_transaction_details_table', 8),
(11, '2019_08_26_160456_create_countries_table', 9),
(12, '2019_08_27_064823_create_item_locations_table', 10),
(13, '2019_08_30_135027_create_categories_table', 11),
(14, '2019_08_30_135612_create_item_categories_table', 12),
(15, '2019_08_31_032215_create_categories_table', 13),
(16, '2019_08_31_073632_create_provinces_table', 14),
(17, '2019_08_31_073652_create_cities_table', 14),
(18, '2019_08_31_083125_create_table_city', 15);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinces`
--

CREATE TABLE `provinces` (
  `id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `provinces`
--

INSERT INTO `provinces` (`id`, `province_id`, `province`) VALUES
(1, 1, 'Bali'),
(2, 2, 'Bangka Belitung'),
(3, 3, 'Banten'),
(4, 4, 'Bengkulu'),
(5, 5, 'DI Yogyakarta'),
(6, 6, 'DKI Jakarta'),
(7, 7, 'Gorontalo'),
(8, 8, 'Jambi'),
(9, 9, 'Jawa Barat'),
(10, 10, 'Jawa Tengah'),
(11, 11, 'Jawa Timur'),
(12, 12, 'Kalimantan Barat'),
(13, 13, 'Kalimantan Selatan'),
(14, 14, 'Kalimantan Tengah'),
(15, 15, 'Kalimantan Timur'),
(16, 16, 'Kalimantan Utara'),
(17, 17, 'Kepulauan Riau'),
(18, 18, 'Lampung'),
(19, 19, 'Maluku'),
(20, 20, 'Maluku Utara'),
(21, 21, 'Nanggroe Aceh Darussalam (NAD)'),
(22, 22, 'Nusa Tenggara Barat (NTB)'),
(23, 23, 'Nusa Tenggara Timur (NTT)'),
(24, 24, 'Papua'),
(25, 25, 'Papua Barat'),
(26, 26, 'Riau'),
(27, 27, 'Sulawesi Barat'),
(28, 28, 'Sulawesi Selatan'),
(29, 29, 'Sulawesi Tengah'),
(30, 30, 'Sulawesi Tenggara'),
(31, 31, 'Sulawesi Utara'),
(32, 32, 'Sumatera Barat'),
(33, 33, 'Sumatera Selatan'),
(34, 34, 'Sumatera Utara');

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`id`, `name`, `content`, `created_at`, `updated_at`) VALUES
(1, 'website', '[{\"name\":\"BON\",\"description\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus dolorum eveniet ipsam quibusdam. Asperiores aspernatur beatae blanditiis, commodi delectus eaque eligendi illo, iusto minima nulla perferendis quia quidem repellendus tempore. Asperiores aspernatur beatae blanditiis, commodi delectus eaque eligendi illo, iusto minima nulla perferendis quia quidem repellendus tempore.<\\/p>\",\"country\":\"Indonesia\",\"address\":\"Jl. Menteng, Jakarta\",\"phone\":\"081200331211\",\"email\":\"info@pst.com\",\"logo\":\"http:\\/\\/localhost:8000\\/img\\/logo\\/LOGO_SedRbQ_back4.png\", \"slogan\": \"Tes\"}]', '2019-07-31 17:00:00', '2019-07-31 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_buyer` smallint(6) NOT NULL,
  `total` int(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transactions`
--

INSERT INTO `transactions` (`id`, `code`, `id_buyer`, `total`, `created_at`, `updated_at`) VALUES
(17, 'TRS_75_BOOK', 14, 2375320, '2019-09-02 00:11:33', '2019-09-02 00:11:33'),
(18, 'TRS_76_BOOK', 15, 106500, '2019-09-02 06:21:56', '2019-09-02 06:21:56'),
(19, 'TRS_76_BOOK', 15, 43000, '2019-09-02 17:47:24', '2019-09-02 17:47:24'),
(20, 'TRS_76_BOOK', 15, 69000, '2019-09-02 17:49:53', '2019-09-02 17:49:53'),
(21, 'TRS_76_BOOK', 15, 69000, '2019-09-02 17:50:48', '2019-09-02 17:50:48'),
(22, 'TRS_76_BOOK', 15, 69000, '2019-09-02 17:51:49', '2019-09-02 17:51:49'),
(23, 'TRS_76_BOOK', 15, 69000, '2019-09-02 17:52:14', '2019-09-02 17:52:14'),
(24, 'TRS_76_BOOK', 15, 69000, '2019-09-02 17:52:37', '2019-09-02 17:52:37'),
(25, 'TRS_76_BOOK', 15, 69000, '2019-09-02 17:52:47', '2019-09-02 17:52:47'),
(26, 'TRS_76_BOOK', 15, 69000, '2019-09-02 17:53:01', '2019-09-02 17:53:01'),
(27, 'TRS_76_BOOK', 15, 69000, '2019-09-02 17:53:06', '2019-09-02 17:53:06'),
(28, 'TRS_76_BOOK', 15, 69000, '2019-09-02 17:57:20', '2019-09-02 17:57:20'),
(29, 'TRS_76_BOOK', 15, 69000, '2019-09-02 17:57:30', '2019-09-02 17:57:30'),
(30, 'TRS_76_BOOK', 15, 69000, '2019-09-02 17:57:50', '2019-09-02 17:57:50'),
(31, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:01:14', '2019-09-02 18:01:14'),
(32, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:01:36', '2019-09-02 18:01:36'),
(33, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:01:50', '2019-09-02 18:01:50'),
(34, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:03:16', '2019-09-02 18:03:16'),
(35, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:03:27', '2019-09-02 18:03:27'),
(36, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:05:14', '2019-09-02 18:05:14'),
(37, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:05:20', '2019-09-02 18:05:20'),
(38, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:05:57', '2019-09-02 18:05:57'),
(39, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:07:10', '2019-09-02 18:07:10'),
(40, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:07:21', '2019-09-02 18:07:21'),
(41, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:09:11', '2019-09-02 18:09:11'),
(42, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:09:34', '2019-09-02 18:09:34'),
(43, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:11:53', '2019-09-02 18:11:53'),
(44, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:12:38', '2019-09-02 18:12:38'),
(45, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:12:51', '2019-09-02 18:12:51'),
(46, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:15:00', '2019-09-02 18:15:00'),
(47, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:15:16', '2019-09-02 18:15:16'),
(48, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:15:27', '2019-09-02 18:15:27'),
(49, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:15:36', '2019-09-02 18:15:36'),
(50, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:16:09', '2019-09-02 18:16:09'),
(51, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:16:28', '2019-09-02 18:16:28'),
(52, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:17:12', '2019-09-02 18:17:12'),
(53, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:20:11', '2019-09-02 18:20:11'),
(54, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:20:23', '2019-09-02 18:20:23'),
(55, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:21:29', '2019-09-02 18:21:29'),
(56, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:21:37', '2019-09-02 18:21:37'),
(57, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:30:54', '2019-09-02 18:30:54'),
(58, 'TRS_76_BOOK', 15, 69000, '2019-09-02 18:33:18', '2019-09-02 18:33:18'),
(59, 'TRS_76_BOOK', 15, 69000, '2019-09-02 19:14:33', '2019-09-02 19:14:33'),
(60, 'TRS_75_BOOK', 15, 494464, '2019-09-02 19:49:08', '2019-09-02 19:49:08'),
(61, 'TRS_75_BOOK', 15, 507964, '2019-09-02 19:53:36', '2019-09-02 19:53:36'),
(62, 'TRS_75_BOOK', 15, 494464, '2019-09-02 19:54:23', '2019-09-02 19:54:23'),
(63, 'TRS_75_BOOK', 15, 494464, '2019-09-02 19:54:23', '2019-09-02 19:54:23'),
(64, 'TRS_75_BOOK', 15, 494464, '2019-09-02 19:54:23', '2019-09-02 19:54:23'),
(65, 'TRS_75_BOOK', 15, 494464, '2019-09-02 19:54:24', '2019-09-02 19:54:24'),
(66, 'TRS_75_BOOK', 15, 494464, '2019-09-02 19:54:24', '2019-09-02 19:54:24'),
(67, 'TRS_75_BOOK', 15, 494464, '2019-09-02 19:54:24', '2019-09-02 19:54:24'),
(68, 'TRS_75_BOOK', 15, 507964, '2019-09-02 19:57:40', '2019-09-02 19:57:40'),
(69, 'TRS_75_BOOK', 15, 507964, '2019-09-02 20:08:58', '2019-09-02 20:08:58'),
(70, 'TRS_75_BOOK2019_09_03', 15, 261232, '2019-09-02 21:06:02', '2019-09-02 21:06:02'),
(71, 'TRS_75_BOOK_2019_09_03_20', 15, 261232, '2019-09-02 21:10:20', '2019-09-02 21:10:20'),
(72, 'TRS_76_BOOK_2019_09_03_02', 15, 246500, '2019-09-02 22:42:02', '2019-09-02 22:42:02'),
(73, 'TRS_75_BOOK_2019_09_03_51', 15, 749696, '2019-09-02 23:37:51', '2019-09-02 23:37:51'),
(74, 'TRS_75_BOOK_2019_09_03_41', 15, 749696, '2019-09-03 00:10:41', '2019-09-03 00:10:41'),
(75, 'TRS_76_BOOK_2019_09_03_25', 15, 121000, '2019-09-03 00:17:25', '2019-09-03 00:17:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaction_details`
--

CREATE TABLE `transaction_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_transaction` int(10) UNSIGNED NOT NULL,
  `id_item` int(6) UNSIGNED NOT NULL,
  `id_owner` smallint(6) NOT NULL,
  `price` int(20) NOT NULL,
  `quantity` smallint(6) NOT NULL,
  `send_address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transaction_details`
--

INSERT INTO `transaction_details` (`id`, `id_transaction`, `id_item`, `id_owner`, `price`, `quantity`, `send_address`, `status`, `created_at`, `updated_at`) VALUES
(4, 17, 75, 3, 233232, 10, '[{\"country\":\"Indonesia\",\"province\":\"11\",\"city\":\"487\",\"postal_code\":\"66311\",\"number_phone\":\"092336332207\",\"detail\":\"JANGAN DIBANTING YAA MASS, NANTI RETAK\"}]', 1, '2019-09-02 00:11:33', '2019-09-02 06:22:51'),
(5, 18, 76, 3, 20000, 3, '[{\"country\":\"Indonesia\",\"province\":\"11\",\"city\":\"487\",\"postal_code\":\"66312\",\"number_phone\":\"121332434\",\"detail\":\"no special description\"}]', 0, '2019-09-02 06:21:56', '2019-09-02 06:21:56'),
(6, 19, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"5\",\"city\":\"135\",\"postal_code\":\"55812\",\"number_phone\":\"2343243242\",\"detail\":\"sdffdsfdsfsfs\"}]', 0, '2019-09-02 17:47:24', '2019-09-02 17:47:24'),
(7, 20, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 17:49:53', '2019-09-02 17:49:53'),
(8, 21, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 17:50:48', '2019-09-02 17:50:48'),
(9, 22, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 17:51:49', '2019-09-02 17:51:49'),
(10, 23, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 17:52:14', '2019-09-02 17:52:14'),
(11, 24, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 17:52:37', '2019-09-02 17:52:37'),
(12, 25, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 17:52:47', '2019-09-02 17:52:47'),
(13, 26, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 17:53:01', '2019-09-02 17:53:01'),
(14, 27, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 17:53:06', '2019-09-02 17:53:06'),
(15, 28, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 17:57:20', '2019-09-02 17:57:20'),
(16, 29, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 17:57:30', '2019-09-02 17:57:30'),
(17, 30, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 17:57:50', '2019-09-02 17:57:50'),
(18, 31, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:01:14', '2019-09-02 18:01:14'),
(19, 32, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:01:36', '2019-09-02 18:01:36'),
(20, 33, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:01:50', '2019-09-02 18:01:50'),
(21, 34, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:03:16', '2019-09-02 18:03:16'),
(22, 35, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:03:27', '2019-09-02 18:03:27'),
(23, 36, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:05:14', '2019-09-02 18:05:14'),
(24, 37, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:05:20', '2019-09-02 18:05:20'),
(25, 38, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:05:57', '2019-09-02 18:05:57'),
(26, 39, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:07:10', '2019-09-02 18:07:10'),
(27, 40, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:07:21', '2019-09-02 18:07:21'),
(28, 41, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:09:11', '2019-09-02 18:09:11'),
(29, 42, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:09:34', '2019-09-02 18:09:34'),
(30, 43, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:11:53', '2019-09-02 18:11:53'),
(31, 44, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:12:38', '2019-09-02 18:12:38'),
(32, 45, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:12:51', '2019-09-02 18:12:51'),
(33, 46, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:15:00', '2019-09-02 18:15:00'),
(34, 47, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:15:16', '2019-09-02 18:15:16'),
(35, 48, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:15:27', '2019-09-02 18:15:27'),
(36, 49, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:15:36', '2019-09-02 18:15:36'),
(37, 50, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:16:09', '2019-09-02 18:16:09'),
(38, 51, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:16:28', '2019-09-02 18:16:28'),
(39, 52, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:17:12', '2019-09-02 18:17:12'),
(40, 53, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:20:11', '2019-09-02 18:20:11'),
(41, 54, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:20:23', '2019-09-02 18:20:23'),
(42, 55, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:21:29', '2019-09-02 18:21:29'),
(43, 56, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:21:37', '2019-09-02 18:21:37'),
(44, 57, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:30:54', '2019-09-02 18:30:54'),
(45, 58, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 18:33:18', '2019-09-02 18:33:18'),
(46, 59, 76, 3, 20000, 1, '[{\"country\":\"Indonesia\",\"province\":\"14\",\"city\":\"167\",\"postal_code\":\"73583\",\"number_phone\":\"2343243242\",\"detail\":\"sdfsdfsfdsfds\"}]', 0, '2019-09-02 19:14:33', '2019-09-02 19:14:33'),
(47, 60, 75, 3, 233232, 2, '[{\"country\":\"Andorra\",\"province\":\"6\",\"city\":\"154\",\"postal_code\":\"13330\",\"number_phone\":\"324234\",\"detail\":\"wqeqeqwewqdasd\"}]', 0, '2019-09-02 19:49:08', '2019-09-02 19:49:08'),
(48, 61, 75, 3, 233232, 2, '[{\"country\":\"Indonesia\",\"province\":\"6\",\"city\":\"153\",\"postal_code\":\"12230\",\"number_phone\":\"2342432\",\"detail\":\"sadsadsadsa\"}]', 0, '2019-09-02 19:53:36', '2019-09-02 19:53:36'),
(49, 62, 75, 3, 233232, 2, '[{\"country\":\"Andorra\",\"province\":\"6\",\"city\":\"154\",\"postal_code\":\"13330\",\"number_phone\":\"324234\",\"detail\":\"wqeqeqwewqdasd\"}]', 0, '2019-09-02 19:54:23', '2019-09-02 19:54:23'),
(50, 63, 75, 3, 233232, 2, '[{\"country\":\"Andorra\",\"province\":\"6\",\"city\":\"154\",\"postal_code\":\"13330\",\"number_phone\":\"324234\",\"detail\":\"wqeqeqwewqdasd\"}]', 0, '2019-09-02 19:54:23', '2019-09-02 19:54:23'),
(51, 64, 75, 3, 233232, 2, '[{\"country\":\"Andorra\",\"province\":\"6\",\"city\":\"154\",\"postal_code\":\"13330\",\"number_phone\":\"324234\",\"detail\":\"wqeqeqwewqdasd\"}]', 0, '2019-09-02 19:54:23', '2019-09-02 19:54:23'),
(52, 65, 75, 3, 233232, 2, '[{\"country\":\"Andorra\",\"province\":\"6\",\"city\":\"154\",\"postal_code\":\"13330\",\"number_phone\":\"324234\",\"detail\":\"wqeqeqwewqdasd\"}]', 0, '2019-09-02 19:54:24', '2019-09-02 19:54:24'),
(53, 66, 75, 3, 233232, 2, '[{\"country\":\"Andorra\",\"province\":\"6\",\"city\":\"154\",\"postal_code\":\"13330\",\"number_phone\":\"324234\",\"detail\":\"wqeqeqwewqdasd\"}]', 0, '2019-09-02 19:54:24', '2019-09-02 19:54:24'),
(54, 67, 75, 3, 233232, 2, '[{\"country\":\"Andorra\",\"province\":\"6\",\"city\":\"154\",\"postal_code\":\"13330\",\"number_phone\":\"324234\",\"detail\":\"wqeqeqwewqdasd\"}]', 0, '2019-09-02 19:54:24', '2019-09-02 19:54:24'),
(55, 68, 75, 3, 233232, 2, '[{\"country\":\"Indonesia\",\"province\":\"6\",\"city\":\"153\",\"postal_code\":\"12230\",\"number_phone\":\"2342432\",\"detail\":\"sadsadsadsa\"}]', 0, '2019-09-02 19:57:40', '2019-09-02 19:57:40'),
(56, 69, 75, 3, 233232, 2, '[{\"country\":\"Indonesia\",\"province\":\"6\",\"city\":\"153\",\"postal_code\":\"12230\",\"number_phone\":\"2342432\",\"detail\":\"sadsadsadsa\"}]', 0, '2019-09-02 20:08:58', '2019-09-02 20:08:58'),
(57, 70, 75, 3, 233232, 1, '[{\"country\":\"Indonesia\",\"province\":\"6\",\"city\":\"154\",\"postal_code\":\"13330\",\"number_phone\":\"323424\",\"detail\":\"dsfasd\"}]', 0, '2019-09-02 21:06:02', '2019-09-02 21:06:02'),
(58, 71, 75, 3, 233232, 1, '[{\"country\":\"Indonesia\",\"province\":\"6\",\"city\":\"154\",\"postal_code\":\"13330\",\"number_phone\":\"323424\",\"detail\":\"dsfasd\"}]', 0, '2019-09-02 21:10:20', '2019-09-02 21:10:20'),
(59, 72, 76, 3, 20000, 10, '[{\"country\":\"Indonesia\",\"province\":\"11\",\"city\":\"487\",\"postal_code\":\"66312\",\"number_phone\":\"12313123123\",\"detail\":\"Jangan dibuka dulu !!! biar aku saja !!\"}]', 1, '2019-09-02 22:42:02', '2019-09-02 22:43:09'),
(60, 73, 75, 3, 233232, 3, '[{\"country\":\"Indonesia\",\"province\":\"11\",\"city\":\"75\",\"postal_code\":\"66124\",\"number_phone\":\"2342423423\",\"detail\":null}]', 0, '2019-09-02 23:37:51', '2019-09-02 23:37:51'),
(61, 74, 75, 3, 233232, 3, '[{\"country\":\"Indonesia\",\"province\":\"11\",\"city\":\"75\",\"postal_code\":\"66124\",\"number_phone\":\"2342423423\",\"detail\":null}]', 0, '2019-09-03 00:10:41', '2019-09-03 00:10:41'),
(62, 75, 76, 3, 20000, 4, '[{\"country\":\"Indonesia\",\"province\":\"10\",\"city\":\"134\",\"postal_code\":\"58111\",\"number_phone\":\"12313213123\",\"detail\":\"nodaosndoasndsaod\"}]', 0, '2019-09-03 00:17:25', '2019-09-03 00:17:25');

--
-- Trigger `transaction_details`
--
DELIMITER $$
CREATE TRIGGER `after_transaction` AFTER INSERT ON `transaction_details` FOR EACH ROW begin
UPDATE items
set quantity = quantity - new.quantity
where id = new.id_item;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT 0,
  `bio` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `avatar`, `role`, `bio`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Admin Admin', 'admin@admin.com', '2019-07-24 21:28:06', '$2y$10$Ur4UHJ3yo4Qm/3zALte6Ke7s3o81j3fqBDsCNo.c1zqRn0FaIXNB.', NULL, 0, NULL, NULL, '2019-07-24 21:28:06', '2019-07-24 21:40:54'),
(3, 'Ahmad Maulidinss', 'maulidinahmadfurqon@gmail.com', NULL, '$2y$10$sRoLRGQ6YFPws.Qx3JQCc.ipTAJANee5Bj.mP/3MvweuQZqlOyqOy', NULL, 0, NULL, NULL, '2019-07-24 21:44:01', '2019-08-21 04:51:30'),
(8, 'Ahmad M', 'maulidinahmadfurqon+9@gmail.com', NULL, '$2y$10$b6R/pMRwCcGqhzma/AgeXedk9eJo6iGLhbEPKK.HTii9B.feY.Lr.', NULL, 0, NULL, NULL, '2019-08-06 00:25:54', '2019-08-06 00:25:54'),
(9, 'Ahmad F', 'maulidinahmadfurqon+23@gmail.com', NULL, '$2y$10$GNWdNsaXyj7hTsJMJX3DVu/9.h054mjRz9bw3LbHj0R7xna08QFwy', NULL, 0, NULL, NULL, '2019-08-06 01:02:16', '2019-08-06 01:02:16'),
(10, 'Ahmad M', 'maulidinahmadfurqon+24@gmail.com', NULL, '$2y$10$ZnY5P7oDmlZtkKL8J9HHTeIs7aDtUwWTlOdsERnk9hFuOWtze69Ka', NULL, 0, NULL, NULL, '2019-08-07 19:06:44', '2019-08-07 19:06:44'),
(11, 'AHmad Coba', 'maulidinahmadfurqon+26@gmail.com', NULL, '$2y$10$usKKqUTObE4hyL30Uep96Ou5C9wAR3wvaxyWDt4dqauJBL1Gaa8VC', NULL, 0, NULL, NULL, '2019-08-07 21:42:20', '2019-08-07 21:42:20'),
(12, 'user', 'user@user.com', NULL, '$2y$10$CQV/pLmUTCBNjwfqoMvyYephT5WprXV1ymapr4x/3OZwIsGgtgmx2', NULL, 0, NULL, NULL, '2019-08-26 04:25:21', '2019-08-26 04:25:21'),
(13, 'Ahmad Maulidin', 'maulidinahmadfurqon+1@gmail.com', NULL, '$2y$10$WZ50px3e4PJ3shYZWLMD/.G8k58p2tWD9D9SbBqTCWWtN8vK.s5fC', NULL, 0, NULL, NULL, '2019-08-28 07:16:26', '2019-08-28 07:16:26'),
(14, 'Aldi', 'aldirianto@gmail.com', NULL, '$2y$10$Y6rzMH70SbDaVRDgURHs1OB7K/nkjcHX6skyFjAp6iGWDrCqh.mee', NULL, 0, NULL, NULL, '2019-09-02 00:07:53', '2019-09-02 00:07:53'),
(15, 'AHmad sss', 'maulidinahmadfurqon+13@gmail.com', NULL, '$2y$10$1Ak5yynUSqMJwOhP4nmJ6uNKIJ0ezBIXMn6vYCEN35YS4oqRNMg.6', NULL, 0, NULL, NULL, '2019-09-02 06:20:58', '2019-09-02 06:20:58');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indeks untuk tabel `cart_storage`
--
ALTER TABLE `cart_storage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_storage_id_index` (`id`);

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `item_categories`
--
ALTER TABLE `item_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_item` (`id_item`,`id_category`),
  ADD KEY `item_locations_ibfk_2` (`id_category`);

--
-- Indeks untuk tabel `item_locations`
--
ALTER TABLE `item_locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_item_2` (`id_item`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `province_id` (`province_id`),
  ADD KEY `city_id` (`city_id`);

--
-- Indeks untuk tabel `item_photos`
--
ALTER TABLE `item_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_item` (`id_item`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaction_details`
--
ALTER TABLE `transaction_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_transaction` (`id_transaction`),
  ADD KEY `id_item` (`id_item`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;

--
-- AUTO_INCREMENT untuk tabel `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT untuk tabel `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT untuk tabel `item_categories`
--
ALTER TABLE `item_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT untuk tabel `item_locations`
--
ALTER TABLE `item_locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT untuk tabel `item_photos`
--
ALTER TABLE `item_photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT untuk tabel `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT untuk tabel `transaction_details`
--
ALTER TABLE `transaction_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `item_categories`
--
ALTER TABLE `item_categories`
  ADD CONSTRAINT `fk_items_ibfk_2` FOREIGN KEY (`id_item`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `item_locations_ibfk_2` FOREIGN KEY (`id_category`) REFERENCES `categories` (`id`);

--
-- Ketidakleluasaan untuk tabel `item_locations`
--
ALTER TABLE `item_locations`
  ADD CONSTRAINT `item_loc_fk_4` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `item_loc_fk_5` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`),
  ADD CONSTRAINT `item_loc_fk_6` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `item_locations_ibfk_1` FOREIGN KEY (`id_item`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `item_photos`
--
ALTER TABLE `item_photos`
  ADD CONSTRAINT `item_photos_ibfk_1` FOREIGN KEY (`id_item`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `transaction_details`
--
ALTER TABLE `transaction_details`
  ADD CONSTRAINT `fk_transaksi` FOREIGN KEY (`id_transaction`) REFERENCES `transactions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_transaksi_2` FOREIGN KEY (`id_item`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
